"""
High-level tests for the  package.

"""

import sps_blowup


def test_version():
    assert sps_blowup.__version__ is not None
