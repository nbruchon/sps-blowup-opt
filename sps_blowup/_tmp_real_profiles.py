# Automated Optimization of Controlled Longitudinal Emittance Blow-Up
# Developed by N. Bruchon in 2022

# import
import logging
import typing as t
import os
import numpy as np

from cernml import japc_utils
from cernml.coi import cancellation

# pyjapc
from pyjapc import PyJapc
import pjlsa

# For script debugging
import pickle
import matplotlib.pyplot as plt
from datetime import datetime

# logging config
logging.basicConfig(level=logging.INFO)
LOG = logging.getLogger(__name__)


# Class
class RealProfiles:
    """
    Class to collect longitudinal profiles from a FESA class

    Attributes
    ----------

    Methods
    -------
    """
    SETTINGS: np.ndarray

    # ABWLMSPS
    ################
    DEVICE_ABWLMSPS: str = "ABWLMSPS"
    PROPERTY_LatestAcquisition: str = "LatestAcquisition"
    ATTRIBUTES_LatestAcquisition: t.ClassVar[t.Tuple[str, ...]] = (
        "nBunchesLatest",
        "acqTimeFromInjLatest",
        "waveform",
        "bunchPositionsLatest",
    )

    PROPERTY_PresentSetUp: str = "PresentSetUp"
    ATTRIBUTES_PresentSetUp: t.ClassVar[t.Tuple[str, ...]] = (
        "acquisitionTimeOutDelayPresent",
        "opTimeDelayPresent",
    )

    PROPERTU_Acquisition: str = "Acquisition"
    ATTRIBUTES_Acquisition: t.ClassVar[t.Tuple[str, ...]] = (
        "acqTimeFromInj",
        "bunchLengthMin",
        "bunchLengthMean",
        "bunchLengthMax"
    )
    ################

    # SPSBQMSPSv1
    ################
    DEVICE_SPSBQMSPSv1: str = "SPSBQMSPSv1"
    PROPERTY_InjectionQuality: str = "InjectionQuality"
    ATTRIBUTES_InjectionQuality: t.ClassVar[t.Tuple[str, ...]] = (
        "bunchLengthInjectionMax",
        "bunchLengthInjectionMaxThr",
        "bunchLengthInjectionMin",
        "bunchLengthInjectionMinThr",
    )
    ################

    def __init__(self,
                 injection: float,
                 extraction: float,
                 nbunches: int,
                 observation_points: np.ndarray,
                 japc: "PyJapc",
                 token: cancellation.Token
                 ) -> None:
        """

        """
        LOG.warning(f"CONNECTING TO {japc.getSelector()}")

        self.LatestAcquisition_address = os.path.join(self.DEVICE_ABWLMSPS, self.PROPERTY_LatestAcquisition)
        self.PresentSetup_address = os.path.join(self.DEVICE_ABWLMSPS, self.PROPERTY_PresentSetUp)
        self.Acquisition_address = os.path.join(self.DEVICE_ABWLMSPS, self.PROPERTU_Acquisition)
        self.InjectionQuality_address = os.path.join(self.DEVICE_SPSBQMSPSv1, self.PROPERTY_InjectionQuality)

        self.japc = japc
        self.token = token
        self._cycle_based_times = observation_points

        # Subscribe to streams and start monitoring

        # LatestAcquisition
        LatestAcquisition_streams = self._get_streams(
            self.LatestAcquisition_address,
            self.ATTRIBUTES_LatestAcquisition
        )
        self.LatestAcquisition_streams = self._open(
            streams=LatestAcquisition_streams,
            attributes=self.ATTRIBUTES_LatestAcquisition
        )

        # PresentSetUp
        PresentSetUp_streams = self._get_streams(
            self.PresentSetup_address,
            self.ATTRIBUTES_PresentSetUp
        )
        self.PresentSetUp_streams = self._open(
            streams=PresentSetUp_streams,
            attributes=self.ATTRIBUTES_PresentSetUp
        )

        # Acquisition
        Acquisition_strams = self._get_streams(
            self.Acquisition_address,
            self.ATTRIBUTES_Acquisition
        )
        self.Acquisition_streams = self._open(
            streams=Acquisition_strams,
            attributes=self.ATTRIBUTES_Acquisition
        )

        # InjectionQuality
        InjectionQuality_streams = self._get_streams(
            self.InjectionQuality_address,
            self.ATTRIBUTES_InjectionQuality
        )
        self.InjectionQuality_streams = self._open(
            streams=InjectionQuality_streams,
            attributes=self.ATTRIBUTES_InjectionQuality
        )

        # Stream objects: LatestAcquisition
        self.stream_nBunches = self.LatestAcquisition_streams[self.ATTRIBUTES_LatestAcquisition[0]]
        self.stream_acqTimeFromInj = self.LatestAcquisition_streams[self.ATTRIBUTES_LatestAcquisition[1]]
        self.stream_waveform = self.LatestAcquisition_streams[self.ATTRIBUTES_LatestAcquisition[2]]
        self.stream_bunchPosition = self.LatestAcquisition_streams[self.ATTRIBUTES_LatestAcquisition[3]]

        # Stream objects: PresentSetUp
        self.stream_acquisitionTimeOutDelayPresent = self.PresentSetUp_streams[self.ATTRIBUTES_PresentSetUp[0]]
        self.stream_opTimeDelayPresent = self.PresentSetUp_streams[self.ATTRIBUTES_PresentSetUp[1]]

        # Stream objects: Acquisition
        self.stream_acqTimeFromInj_ = self.Acquisition_streams[self.ATTRIBUTES_Acquisition[0]]
        self.stream_bunchLengthMin_ = self.Acquisition_streams[self.ATTRIBUTES_Acquisition[1]]
        self.stream_bunchLengthMean_ = self.Acquisition_streams[self.ATTRIBUTES_Acquisition[2]]
        self.stream_bunchLengthMax_ = self.Acquisition_streams[self.ATTRIBUTES_Acquisition[3]]

        # Stream objects: InjectionQuality
        self.stream_bunchLengthInjectionMax = self.InjectionQuality_streams[self.ATTRIBUTES_InjectionQuality[0]]
        self.stream_bunchLengthInjectionMaxThr = self.InjectionQuality_streams[self.ATTRIBUTES_InjectionQuality[1]]
        self.stream_bunchLengthInjectionMin = self.InjectionQuality_streams[self.ATTRIBUTES_InjectionQuality[2]]
        self.stream_bunchLengthInjectionMinThr = self.InjectionQuality_streams[self.ATTRIBUTES_InjectionQuality[3]]

        self.resolution = self._get_resolution()
        # add by default 100ms to injection --> BQM has time for analysis
        injection_time = injection + 100
        self.injection = self._cycle_time_resolution(cycle_time=injection_time)
        self.extraction = self._cycle_time_resolution(cycle_time=extraction)

        self.nbunches = nbunches

    def _get_streams(self, address: str, attributes: t.Tuple):
        streams_dict = {}
        for attr in attributes:
            attr_address = '#'.join([address, attr])
            attr_stream = japc_utils.subscribe_stream(
                japc=self.japc,
                name_or_names=attr_address,
                token=self.token
            )
            streams_dict[attr] = attr_stream
        return streams_dict

    @staticmethod
    def _open(streams: dict, attributes: t.Tuple) -> dict:
        """

        """
        for attr in attributes:
            streams[attr].start_monitoring()
        return streams

    def _get_resolution(self):
        resolution, resolution_info = self.stream_opTimeDelayPresent.pop_or_wait()
        return resolution

    def _close(self) -> None:
        """

        """
        for attr in self.ATTRIBUTES_LatestAcquisition:
            self.LatestAcquisition_streams[attr].stop_monitoring()

        for attr in self.ATTRIBUTES_PresentSetUp:
            self.PresentSetUp_streams[attr].stop_monitoring()

        for attr in self.ATTRIBUTES_InjectionQuality:
            self.InjectionQuality_streams[attr].stop_monitoring()

        return None

    def _injection_check(self):
        """ """

        # Read max. bunch length at injection
        inj_max_bl, inj_max_bl_info = self.stream_bunchLengthInjectionMax.pop_or_wait()
        LOG.info(f"bunchLengthInjectionMax: {inj_max_bl}")

        # Read threshold on max. bunch length at injection
        inj_max_bl_th, inj_max_bl_th_info = self.stream_bunchLengthInjectionMaxThr.pop_if_ready()
        LOG.info(f"bunchLengthInjectionMaxThr: {inj_max_bl_th}")

        # Read min. bunch length at injection
        inj_min_bl, inj_min_bl_info = self.stream_bunchLengthInjectionMin.pop_if_ready()
        LOG.info(f"bunchLengthInjectionMin: {inj_min_bl}")

        # Read threshold on min. bunch length at injection
        inj_min_bl_th, inj_min_bl_th_info = self.stream_bunchLengthInjectionMinThr.pop_if_ready()
        LOG.info(f"bunchLengthInjectionMinThr: {inj_min_bl_th}")

        inj_info = {
            "bunchLengthInjectionMax": inj_max_bl,
            "bunchLengthInjectionMaxThr": inj_max_bl_th,
            "bunchLengthInjectionMin": inj_min_bl,
            "bunchLengthInjectionMinThr": inj_min_bl_th
        }
        #
        n_bunches, n_bunches_info = self.stream_nBunches.pop_if_ready()
        LOG.info(f"nbunches at last injection: {n_bunches}")

        #
        if n_bunches == 0:
            LOG.info(f"NO BEAM DETECTED \n\nWaiting for next cycle...\n")
            check = False
        elif inj_max_bl > inj_max_bl_th:
            LOG.info(f"INJECTION BUNCHLENGTH HIGHER THAN MAX THRESHOLD \n\nWaiting for next cycle...\n")
            check = False
        elif inj_min_bl < inj_min_bl_th:
            LOG.info(f"INJECTION BUNCHLENGTH LOWER THAN MIN THRESHOLD \n\nWaiting for next cycle...\n")
            check = False
        else:
            LOG.warning(f"ACQUISITION ENABLED")
            check = True

        return check, inj_info

    def get_measurements(self, index: int) -> dict:
        """

        """

        assert self.stream_acqTimeFromInj.monitoring

        acquire = True
        beam_check_request = True
        injection_check = False

        beam_info = {}

        cnt = 0

        while acquire:

            acq_time, acq_time_info = self.stream_acqTimeFromInj.wait_for_next()

            if acq_time == self.injection:
                injection_check, injection_info = self._injection_check()
                beam_check_request = False

            if acq_time == self.extraction and not beam_check_request:
                beam_check_request = True

            if (acq_time == self.injection_based_times[cnt]) and injection_check:
                n_bunches, n_bunches_info = self.stream_nBunches.pop_or_wait()
                LOG.info(f"Time from injection: {acq_time}, nbunches: {n_bunches}")

                if n_bunches == 0 and cnt <= index:
                    LOG.warning(f"NO BEAM DETECTED - check attenuation\n\nWaiting for next cycle...\n")

                    acquire = True

                elif n_bunches != self.nbunches and n_bunches != 0:
                    LOG.warning(f"WRONG NUMBER OF BUNCHES\n\nWaiting for next cycle...\n")

                    acquire = True

                else:
                    LOG.warning(f"ACQUIRING DATA")
                    waveform, waveform_info = self.stream_waveform.pop_or_wait()
                    LOG.info(f"Waveform acquired")

                    bunch_position, bunch_position_info = self.stream_bunchPosition.pop_or_wait()
                    LOG.info(f"Bunch position acquired")

                    if n_bunches == 1:
                        bunch_position = np.array([bunch_position])

                    beam_info[str(cnt)] = {
                        "acquisition_time": acq_time,
                        "n_bunches": n_bunches,
                        "waveform": waveform,
                        "bunch_position": bunch_position,
                        **injection_info,
                    }
                    cnt += 1

                if cnt == len(self.injection_based_times):
                    acquire = False

        return beam_info

    def _cycle_time_resolution(self, cycle_time: float) -> float:
        """

        """
        cycle_time = np.ceil(cycle_time / self.resolution) * self.resolution
        return cycle_time

    @property
    def injection_based_times(self) -> t.Tuple[int, ...]:
        observations = np.asarray(
            np.floor(self._cycle_based_times / self.resolution) * self.resolution,
            dtype=np.int64
        )
        return tuple(observations)

    @property
    def _all_bunchlengths(self) -> t.Tuple[list, ...]:
        """ """
        acq_times, acq_times_info = self.stream_acqTimeFromInj_.wait_for_next()  # pop_or_wait()
        bunch_length_min, bunch_length_min_info = self.stream_bunchLengthMin_.pop_or_wait()
        bunch_length_mean, bunch_length_mean_info = self.stream_bunchLengthMean_.pop_or_wait()
        bunch_length_max, bunch_length_max_info = self.stream_bunchLengthMax_.pop_or_wait()

        return acq_times, bunch_length_min, bunch_length_mean, bunch_length_max

    @staticmethod
    def set_setting(settings: np.ndarray) -> None:
        LOG.info(f"Blow-Up settings: {settings}")
        return None


if __name__ == "__main__":

    lsa_client = pjlsa.LSAClient("NEXT")
    with lsa_client.java_api():
        from cernml import lsa_utils

        machine = "SPS"
        user = "USER"
        cycle = "LHC1"  # "LHCMD3"  # "LHC25NS"  # "LHC2"  # "LHCMD1"  # MD1"
        selector = ".".join([machine, user, cycle])
        my_japc = PyJapc(selector=selector)

        incorporator = lsa_utils.Incorporator('SA.BlowUpGenerator/Amplitude#amplitude', user=selector)
        context = incorporator.context
        attributes = lsa_utils.get_cycle_type_attributes(context)

        first_injection_time = float(attributes["VE:First injection"])
        LOG.info(f"First injection: {first_injection_time}")

        injection_time = float(attributes["lastInjectionTimeMs"])
        LOG.info(f'Last injection -> {injection_time}')

        startramp_time = float(attributes["VE:Start ramp 1"])
        LOG.info(f"Ramp start: {startramp_time}")

        flattopstart_time = float(attributes["VE:Start flat top"])
        LOG.info(f"Flat top start -> {flattopstart_time}")

        beamdump_time = float(attributes["VE:Beam dump"])
        LOG.info(f"Beam dump -> {beamdump_time}")

        flattopend_time = float(attributes["flatTopEndTimeMs"])
        LOG.info(f'Flat top end -> {flattopend_time}')

        # selector check
        if selector == my_japc.getSelector():
            LOG.info(f"Selector: {selector}")

        cancellation_token = cancellation.Token()

        n_intervals = 5

        connect_to_SPS = True  # False
        save = True  # False
        if connect_to_SPS:
            # create filename
            now = datetime.now().strftime("%Y_%m_%d_%H_%M_%S_")
            filename = now + cycle + ".pickle"

            if cycle == "LHC1":  # "LHCMD3":  # "LHC25NS":
                start_time = 10015  # 15015.0
                stop_time = 20015  # 19915.0
                rolloff = 100
                n_bunches_requested = 92  # 56

            interval_length = stop_time - rolloff - start_time
            obs = [start_time + step * (interval_length + rolloff) // n_intervals for step in range(1, n_intervals+1)]
            LOG.info(f"Observations: {obs}")

            # t = np.array([1500.0, 2000.0])  # 5000.0
            # measurements
            meas = RealProfiles(
                injection=first_injection_time+250,  # injection_time + 100,
                extraction=beamdump_time,
                nbunches=n_bunches_requested,  # 72,  # 144,  # 72,  # 24,  # 72,
                observation_points=np.array(obs),
                japc=my_japc,
                token=cancellation_token
            )

            _info = meas.get_measurements(index=0)

            if save:
                # d = {"time": _time, "n_bunches": _n_bunches, "waveform": _waveform, "bunch_position": _bunch_position}
                with open(filename, "wb") as handle:
                    pickle.dump(_info, handle, protocol=pickle.HIGHEST_PROTOCOL)

        else:
            # enter filename
            filename = "2023_03_31_16_18_42_LHCMD3.pickle"  # "2022_08_02_18_02_03_LHC1.pickle"
            with open(filename, "rb") as handle:
                _info = pickle.load(handle)

        fig = plt.figure()

        for interval in range(n_intervals):
            ax = fig.add_subplot(1, n_intervals, interval + 1)
            ax.plot(_info[str(interval)]["waveform"])
            ax.set_title(str(_info[str(interval)]['acquisition_time']))
        fig.tight_layout()
        plt.show()

        #
        from _tmp_analysis import Analysis
        an = Analysis()
        n_bunches = _info[str(0)]['n_bunches']
        waveform = _info[str(0)]['waveform']
        bunch_position = _info[str(0)]['bunch_position']

        bunches = an.get_bunches(n_bunches=n_bunches, waveform=waveform, bunch_position=bunch_position)

        bunch_samples = np.shape(bunches)[1]
        timescale = np.arange(bunch_samples) * 0.125e-9

        features_all = []
        for n_bunch in range(n_bunches):
            n_features = an.get_width(timescale, bunches[n_bunch], level=0.5)
            features_all.append(n_features)

        features = np.array(features_all)
        width = features[:, 0]
        center = features[:, 1]
        sigma = features[:, 2]
        bunch_length = features[:, 3]

        ###
        measurements = {}
        # TODO: calcolare features (min, max, mean) nel seguente ciclo for
        for n in list(_info.keys()):
            curr_beam_info = _info[n]
            n_bunches = curr_beam_info["n_bunches"]
            waveform = curr_beam_info["waveform"]
            bunch_position = curr_beam_info["bunch_position"]

            is_single_bunch = False

            if n_bunches == 0:
                LOG.warning(f"No beam at time: {curr_beam_info['acquisition_time']}")
                timescale = np.nan
                bunches = np.nan
                features_all = np.array([[np.nan, np.nan, np.nan, np.nan]])

            else:

                if n_bunches != n_bunches_requested:
                    LOG.warning(f"Incomplete beam at time: {curr_beam_info['acquisition_time']}")

                bunches = an.get_bunches(n_bunches, waveform, bunch_position)

                if n_bunches == 1:
                    is_single_bunch = True

                bunch_samples = np.shape(bunches)[1]
                timescale = np.arange(bunch_samples) * 0.125e-9

                features_all = []
                for n_bunch in range(n_bunches):
                    n_features = an.get_width(timescale, bunches[n_bunch], level=0.5)
                    features_all.append(n_features)

                features_all = np.array(features_all)

            measurements[n] = {
                # "cycle_time": cycle_time,
                **_info[n],
                "is_single_bunch": is_single_bunch,
                "timescale": timescale,
                "bunches": bunches,
                "width": features_all[:, 0],
                "center": features_all[:, 1],
                "sigma": features_all[:, 2],
                "bunch_length": features_all[:, 3],
            }
        ###
        target_sigma = 2.0
        target_tolerance = 0.05
        bunch_tolerance = 0
        _cost = []
        for n in range(n_intervals):
            _measurements = measurements[str(n)]
            bunch_length = _measurements['bunch_length'] * 1e9
            if all(np.isnan(bunch_length)):
                cost = np.nan
            else:
                if not _measurements['is_single_bunch'] and bunch_tolerance > 0:
                    extreme_bl_idxs = np.argsort(np.abs(bunch_length - target_sigma))
                    bunch_length = np.delete(bunch_length, extreme_bl_idxs[-bunch_tolerance])

                outsiders_idxs = np.where(
                    np.logical_or(
                        bunch_length < target_sigma * (1 - target_tolerance),
                        bunch_length > target_sigma * (1 + target_tolerance)
                    )
                )[0]

                bl_out = bunch_length[outsiders_idxs]
                err = bl_out / target_sigma - 1

                cost = np.sqrt(np.sum(err ** 2) / len(bunch_length))
            _cost.append(cost)

        print(_cost)
        ###

        # fig = plt.figure()
        # ax = fig.add_subplot(111)
        # ax.hlines(y=width[0]*1e9, xmin=16015, xmax=_info[str(0)]['acquisition_time'])
        # ax.hlines(y=width[1]*1e9, xmin=_info[str(0)]['acquisition_time'], xmax=_info[str(1)]['acquisition_time'])
        # ax.hlines(y=width[2]*1e9, xmin=_info[str(1)]['acquisition_time'], xmax=_info[str(2)]['acquisition_time'])
        # ax.hlines(y=width[3]*1e9, xmin=_info[str(2)]['acquisition_time'], xmax=_info[str(3)]['acquisition_time'])
        # ax.set(xlim=[15000, 20000])
        # fig.tight_layout()
        # plt.show()
        #
        # start_time = 16015.0
        # stop_time = 19915.0
        # rolloff = 100
        # interval_length = stop_time - rolloff - start_time
        # _obs = [start_time + step * (interval_length + rolloff) // n_intervals for step in range(1, n_intervals + 1)]
        # obs_ = [_info[i]['acquisition_time'] for i in range(len(list(_info.keys())))]
        # print(_obs)
        # print(obs_)
