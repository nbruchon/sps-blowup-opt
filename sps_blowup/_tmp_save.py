import os
import h5py
from h5py._hl.files import File
from h5py._hl.group import Group
from h5py._hl.dataset import Dataset
import logging
from datetime import datetime

import numpy as np

logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger(__name__)


class Save:

    path_to_storage: str = "/afs/cern.ch/user/n/nbruchon/Projects/Storage"
    extension: str = "h5"
    sep: str = '_'

    def __init__(
            self,
            machine: str,
            operation: str,
            cycle: str,
    ):
        self.machine = machine
        self.operation = operation
        self.cycle = cycle

        self.now = self._now
        if not self.check_dir:
            self.create_dir()


    @property
    def _now(self) -> datetime:
        """ """
        return datetime.now()

    @property
    def get_date(self) -> str:
        """ """
        return self.now.strftime("%Y_%m_%d")

    @property
    def get_time(self) -> str:
        """ """
        return self.now.strftime("%H_%M_%S")

    @property
    def dirname(self) -> str:
        """ """
        return self.get_date

    @property
    def dirpath(self) -> str:
        """ """
        return '/'.join([self.path_to_storage, self.dirname])

    @property
    def filename(self):
        name = self.sep.join([self.get_date, self.get_time, self.machine, self.cycle, self.operation])
        return '.'.join([name, self.extension])

    @property
    def filepath(self) -> str:
        """ """
        return '/'.join([self.dirpath, self.filename])

    @property
    def check_file(self) -> bool:
        """ """
        return os.path.isfile(self.filepath)

    @property
    def check_dir(self) -> bool:
        """ """
        return os.path.isdir(self.dirpath)

    def create_dir(self) -> None:
        try:
            os.makedirs(self.dirpath)
        except OSError:
            LOG.warning(f"Impossible to create directory {self.dirpath}.")
        else:
            LOG.info(f"Directory {self.dirpath} successfully created.")
        return None

    @property
    def _get_hdf(self) -> File:
        """ """
        if self.check_file:
            hdf = h5py.File(self.filepath, 'a')
        else:
            hdf = h5py.File(self.filepath, 'w')
            LOG.info(f"File {self.filepath} created.")
        return hdf

    @staticmethod
    def _close_hdf(hdf: File) -> None:
        """ """
        hdf.close()

    def save_dict_to_group(
            self,
            groupname: str,
            dictionary: dict,
    ):
        hdf = self._get_hdf
        if groupname not in list(hdf.keys()):
            hdf_group = hdf.create_group(groupname)
            self.recursively_save_dict(hdf_group, f'/{groupname}/', dictionary)
        else:
            raise ValueError(f"group {groupname} already exists in file {self.filename}")
        self._close_hdf(hdf=hdf)

    def recursively_save_dict(
            self,
            h5group: Group,
            h5groupname: str,
            dictionary: dict,
    ):
        for key, item in dictionary.items():
            if isinstance(item, (int, float, bool, str, tuple, list, np.ndarray)):
                h5group[h5groupname + key] = item
            elif isinstance(item, dict):
                self.recursively_save_dict(h5group, h5groupname + key + '/', item)
            else:
                raise ValueError(f"Cannot save {type(item)} type.")

    def load_dict_from_hdf5(self) -> dict:
        """
        ....
        """
        with h5py.File(self.filepath, 'r') as h5file:
            return self.recursively_load_dict_contents_from_group(h5file, '/')

    def recursively_load_dict_contents_from_group(self, h5file: File, path: str) -> dict:
        """
        ....
        """
        ans = {}
        for key, item in h5file[path].items():
            if isinstance(item, Dataset):
                ans[key] = item[()]
            elif isinstance(item, Group):
                ans[key] = self.recursively_load_dict_contents_from_group(h5file, path + key + '/')
        return ans


if __name__ == "__main__":

    my_machine = "MySPS"
    my_operation = "MyBlowUp"
    my_cycle = "MyCycle"

    sav = Save(
        machine=my_machine,
        operation=my_operation,
        cycle=my_cycle
    )
    filename = sav.filename
    LOG.info(f"filename: {filename}")

    data_0 = {
        "0": {
            "a": np.random.uniform(10),
            "b": True,
            "c": 48,
        },
        "1": {
            "a": np.random.uniform(10),
            "b": False,
            "c": 72,
        }
    }
    groupname = "iter_0"
    sav.save_dict_to_group(groupname=groupname, dictionary=data_0)

    data_1 = {
        "0": {
            "a": np.random.uniform(10),
            "b": True,
            "c": 48,
        },
        "1": {
            "a": np.random.uniform(10),
            "b": False,
            "c": 72,
        }
    }
    groupname = "iter_1"
    sav.save_dict_to_group(groupname=groupname, dictionary=data_1)

    full_dict = sav.load_dict_from_hdf5()
