# Automated Optimization of Controlled Longitudinal Emittance Blow-Up
# Developed by N. Bruchon in 2022

# import
import logging
import typing as t
import os
import numpy as np
import re
import h5py

import itertools

#
# from ._tmp_analysis import Analysis

#
import matplotlib.pyplot as plt


# logging config
logging.basicConfig(level=logging.INFO)
LOG = logging.getLogger(__name__)


# Class
class SimulatedProfiles:
    """
    Class to load longitudinal profiles from available BLonD simulations

    Attributes
    ----------

    Methods
    -------
    """

    SETTINGS:           np.ndarray
    SETTINGS_TO_READ:   dict

    PATH_TO_PROJECT:    str = "/eos/project-s/sps-blowup-ml"
    # PATH_TO_LOCAL:      str = "/afs/cern.ch/user/n/nbruchon/Projects"  # Does not exist anymore...
    SIMULATION_FOLDER:  str = "blond_simulations"
    FILENAME:           str = "h5_profiles.h5"
    PARAMS:             t.ClassVar[t.Tuple[str, ...]] = (
                            "margin_low",
                            "margin_high",
                            "amplitude"
                        )

    resolution: int = 20

    def __init__(self, observation_points: np.ndarray):
        """

        """
        LOG.info(f"Loading profiles from BLonD simulations database")

        self._cycle_based_times = observation_points

        self.path_to_blond_simulations = os.path.join(
            self.PATH_TO_PROJECT,
            self.SIMULATION_FOLDER
        )

        self.path_to_simulations_folder = self.get_path_to_simulations_folder()
        LOG.info(f"Path to simulations folder: {self.path_to_simulations_folder}")

        self.all_settings = self.get_all_settings()
        self.params_range = self.get_params_range()

    def get_path_to_simulations_folder(self):
        path_to_last_simulations_folder = self.get_latest_folder()
        path_to_simulations = os.path.join(path_to_last_simulations_folder, "simulations")
        return path_to_simulations

    def get_latest_folder(self):
        """

        """
        all_folders = [folder for folder in os.listdir(self.path_to_blond_simulations)]
        all_folders.sort()
        last_folder = all_folders[-1]
        path_to_last_folder = os.path.join(self.path_to_blond_simulations, last_folder)
        return path_to_last_folder

    @staticmethod
    def get_settings_from_folder_name(folder_name: str) -> np.array:
        """

        """
        # get a sting with all digits and chars '_' and '.'
        raw_settings_str = re.sub(r'[^_^.\d]', '', folder_name)

        # split the string with respect ot '_' char
        list_settings_str = list(filter(''.__ne__, raw_settings_str.split('_')))

        settings_float = np.array([float(i) for i in list_settings_str])

        # first element unrelated to blow-up settings
        settings = settings_float[1:]

        return settings

    def get_all_settings(self) -> dict:
        """

        """
        sub_folders = os.listdir(self.path_to_simulations_folder)

        settings_dict = {
            dirname: self.get_settings_from_folder_name(dirname) for dirname in sub_folders
        }
        return settings_dict

    def get_available_values(self, settings: np.ndarray) -> dict:
        """

        """
        # if len(settings) != 1:
        #     LOG.warning(f"Unexpected shape of settings array")

        settings_dict = {}
        for idx in range(len(self.PARAMS)):
            value = settings[idx]
            key = self.PARAMS[idx]
            available_values = self.params_range[key]

            # LOG.warning(f"Available values: {available_values}")
            # LOG.warning(f"value: {value}")

            min_val = np.max([i for i in available_values if i <= value])
            max_val = np.min([i for i in available_values if i >= value])

            if min_val == max_val:
                settings_dict[self.PARAMS[idx]] = [min_val]
            else:
                settings_dict[self.PARAMS[idx]] = [min_val, max_val]

        return settings_dict

    def get_params_range(self) -> dict:
        """

        """
        simulated_data_vals = np.vstack(
            list(self.all_settings.values())
        )
        params_range = {}
        for idx in range(len(self.PARAMS)):
            key = self.PARAMS[idx]
            value = np.sort(
                list(
                    set(simulated_data_vals[:, idx])
                    )
                )
            params_range[key] = value
        return params_range

    def get_dirname(self, settings: np.ndarray) -> str:
        """

        """
        for key, val in self.all_settings.items():
            # LOG.info(f"val: {val}")
            if all(val == settings):
                return key
        raise KeyError(f"not found: {val}")

    def get_filepath(self, dirname) -> str:
        """

        """
        dirpath = os.path.join(self.path_to_simulations_folder, dirname)
        fpath = os.path.join(dirpath, 'h5_profiles.h5')
        return fpath

    def set_setting(self, settings: np.ndarray) -> None:
        """ """
        # LOG.info(f"Blow-Up settings: {settings}")
        if len(np.shape(settings)) == 1:
            self.SETTINGS = np.roll(settings, -1)
            # self.SETTINGS_TO_READ = self.get_available_values()
        else:
            self.SETTINGS = np.roll(settings, -1, axis=1)
        return None

    @staticmethod
    def _cycle_time_resolution(cycle_time: float, resolution: int = 20) -> float:
        """

        """
        cycle_time = np.ceil(cycle_time / resolution) * resolution
        return cycle_time

    @staticmethod
    def get_nearest(a: list, threshold: float) -> np.ndarray:
        """

        """
        if threshold in a:
            near_values = [threshold]
        else:
            min_val = np.min(a)
            max_val = np.max(a)
            for val in a:
                if min_val < val < threshold:
                    min_val = val
                if max_val > val > threshold:
                    max_val = val

            near_values = [min_val, max_val]

        return np.array(near_values)

    def _get_measurements(self, cycle_time_idx: int):
        """

        """

        cycle_time = self._cycle_based_times[cycle_time_idx]/1000
        # LOG.info(f"Cycle time: {cycle_time}")

        all_profiles = []

        settings = self.SETTINGS[cycle_time_idx]

        # ranges = list(self.SETTINGS_TO_READ.values())
        ranges_dict = self.get_available_values(settings=settings)
        ranges = list(ranges_dict.values())
        # LOG.info(ranges)

        all_settings = np.vstack(
            list(itertools.product(*ranges))
        )
        # LOG.info(f"ALL SETTINGS: {all_settings}")

        weights = self.get_weights(settings, ranges)

        for i in range(len(all_settings)):
            sim_folder = self.get_dirname(all_settings[i])

            dir_path = os.path.join(self.path_to_simulations_folder, sim_folder)
            file_path = os.path.join(dir_path, self.FILENAME)

            profile = self.read_profile(file_path, cycle_time)
            all_profiles.append(profile[:190])

        all_profiles = np.reshape(all_profiles, (len(ranges[0]), len(ranges[1]), len(ranges[2]), 190))

        final_profile = self.weighted_profile(all_profiles, weights)
        # LOG.info(f"Profile array shape: {np.shape(final_profile)}")

        # TODO: insert the following dictionary in a better way!
        injection_info = {
            "bunchLengthInjectionMax": 0.0,
            "bunchLengthInjectionMaxThr": 0.0,
            "bunchLengthInjectionMin": 0.0,
            "bunchLengthInjectionMinThr": 0.0,
            "settings": settings
        }

        acq_time, n_bunches, waveform, bunch_position = self.get_n_bunches(final_profile, cycle_time)

        return acq_time, n_bunches, waveform, bunch_position, injection_info

    def get_measurements(self, index: int, level: float = 0.5):
        """ """
        beam_info = {}
        for cycle_time_idx in range(len(self._cycle_based_times)):
            acq_time, n_bunches, waveform, bunch_position, injection_info = self._get_measurements(cycle_time_idx)

            beam_info[str(cycle_time_idx)] = {
                "acquisition_time": acq_time,
                "n_bunches": n_bunches,
                "waveform": waveform,
                "bunch_position": bunch_position,
                **injection_info,
            }

        return beam_info


    @staticmethod
    def get_n_bunches(waveform: np.ndarray, cycle_time: float) -> tuple:
        """

        """
        fit_window = 65  # for LHC beams the fit window is 10 ns

        peak_val = []
        peak_idx = []

        # Peaks search
        amp_max = np.max(waveform)
        tmp_peak_idx = 0  # Peak index
        tmp_peak_val = 0  # Peak index
        bunch_cnt = 0

        for i in range(len(waveform)):
            if waveform[i] > 0.3 * amp_max:
                if waveform[i] > tmp_peak_val:
                    tmp_peak_val = waveform[i]
                    tmp_peak_idx = i

            if (tmp_peak_val != 0) and (i + 1 > tmp_peak_idx + 0.75 * fit_window):
                peak_val.append(tmp_peak_val)
                peak_idx.append(tmp_peak_idx)

                bunch_cnt += + 1

                tmp_peak_idx = 0
                tmp_peak_val = 0

        n_bunches = bunch_cnt
        bunch_position = np.array(peak_idx)

        return cycle_time, n_bunches, waveform, bunch_position

    @ staticmethod
    def get_weights(settings, ranges):
        """

        """
        weights = ranges.copy()
        for i in range(len(settings)):
            weights[i] = np.abs(ranges[i] - settings[i]) / np.diff(ranges[i])
            if len(ranges[i]) == 1 and ranges[i] - settings[i] == [0]:
                weights[i] = [1]
        return weights

    def read_profile(self, file_path: str, cycle_time: float):
        """

        """
        with h5py.File(file_path, "r") as f:
            f_keys = list(f.keys())
            k_dict = self.filter_keys(f_keys)
            waveform_times = k_dict['waveform_times']
            waveform_keys = k_dict['waveform_keys']

            # Timescale part just to get the time delta between samples.
            # interval_times = k_dict['interval_times']
            # interval_keys = k_dict['interval_keys']

            values = self.get_nearest(waveform_times, cycle_time)
            # LOG.info(f"Selected time values: {values}")
            indexes = [waveform_times.index(i) for i in values]

            profile_keys = np.array(waveform_keys)[indexes]
            # timescale_keys = np.array(interval_keys)[idxs]
            profiles_list = []
            for idx in range(len(indexes)):
                profile_key = profile_keys[idx]

                profile = f[profile_key][()]
                profiles_list.append(profile[0])

                # timescale_key = timescale_keys[idx]
                # timescale = f[timescale_key][()]

            # LOG.info(f"Delta timescale: {timescale[0][1] - timescale[0][0]}")

            final_profile = np.average(profiles_list, axis=0)

        return final_profile

    @staticmethod
    def filter_keys(keys) -> dict:
        """

        """
        waveform_keys, waveform_times = [], []
        interval_keys, interval_times = [], []
        others = []
        for k in keys:
            if "profiles_matrix_indices" in k:
                others.append(k)
            elif "profiles_matrix" in k:
                waveform_keys.append(k)
                a = re.findall(r"\d+\.*\d*", k)
                waveform_times.append(float(a[-1]))
            elif "profiles_times_matrix" in k:
                interval_keys.append(k)
                a = re.findall(r"\d+\.*\d*", k)
                interval_times.append(float(a[-1]))
            else:
                others.append(k)
        if not (len(waveform_keys) == len(interval_keys) == len(interval_keys) == len(waveform_times)):
            LOG.warning(f"Dictionary items have different lengths!")

        k_dict = {
            'waveform_times': waveform_times,
            'waveform_keys': waveform_keys,
            'interval_times': interval_times,
            'interval_keys': interval_keys
        }
        return k_dict

    @staticmethod
    def weighted_profile(all_profiles, weights):
        """

        """
        avg_freq_high = []
        for i in range(len(weights[0])):
            avg_amp = []
            for j in range(len(weights[1])):
                avg_amp.append(np.average(all_profiles[i, j], axis=0, weights=weights[2]))
            avg_freq_high.append(np.average(avg_amp, axis=0, weights=weights[1]))
        avg_freq_low = np.average(avg_freq_high, axis=0, weights=weights[0])

        avg_profile = avg_freq_low.copy()

        return avg_profile

    @property
    def injection_based_times(self) -> t.Tuple[int, ...]:
        observations = np.asarray(
            np.floor(self._cycle_based_times / self.resolution) * self.resolution,
            dtype=np.int
        )
        return tuple(observations)


if __name__ == "__main__":

    # Simulated profiles
    start_time = 16015.0
    stop_time = 19915.0
    rolloff = 100
    n_intervals = 4

    interval_length = stop_time - rolloff - start_time
    obs = [start_time + step * (interval_length + rolloff) // n_intervals for step in range(n_intervals)]

    # measurements
    sim = SimulatedProfiles(observation_points=np.array(obs))

    def get_settings() -> np.ndarray:
        """ """
        param_values: np.ndarray

        param_values = np.array(
            [
                # 0.0, 0.50, 0.96
                np.random.uniform(0.0, 5.0),
                np.random.uniform(0.5, 0.9),
                np.random.uniform(0.8, 1.1)
            ]
        )
        if param_values[1] > param_values[2]:
            param_values[1] = param_values[2]
            param_values[2] = param_values[1]

        return param_values

    # Define settings
    settings_list = []
    for interval in range(n_intervals):
        settings_list.append(get_settings())
    settings = np.array(settings_list)

    # Send settings to acquisition class
    sim.set_setting(settings)
    LOG.info(f"SETTINGS: {sim.SETTINGS}")

    # Acquire measurements from acquisition class
    _info = sim.get_measurements()

    fig = plt.figure()

    for interval in range(n_intervals):
        ax = fig.add_subplot(1, n_intervals, interval + 1)
        ax.plot(_info[str(interval)]["waveform"])
        ax.set_xlim([50, 125])
        ax.set_title(str(_info[str(interval)]['acquisition_time']))
    fig.tight_layout()
    plt.show()

    # # TODO: wait beam for testing...
    # an = Analysis()
    # _time, _n_bunches, _waveform, _bunch_position = an.get_n_bunches(w, ctime)
    # b = an.get_bunches(_n_bunches, _waveform, _bunch_position)
    # _timescale = np.arange(len(b[0])) * 3.90e-11
    # min_b = np.min(b, axis=0)
    # max_b = np.max(b, axis=0)
    # plt.plot(np.transpose(b))
    # plt.plot(min_b, c='k')
    # plt.plot(max_b, c='k')
    # plt.show()
