"""Utility functions."""

import contextlib
import dataclasses
import typing as t

import numpy as np
from cernml.coi import cancellation
from gym.spaces import Box

CycleTime = t.NewType("CycleTime", float)


@contextlib.contextmanager
def completed_cancellation(token: cancellation.Token) -> t.Iterator[None]:
    """Context manager that completes a cancellation for you."""
    try:
        yield
    except cancellation.CancelledError:
        token.complete_cancellation()
        raise


@dataclasses.dataclass
class HVPair:
    """A pair of arrays, one for each transverse plane."""

    horz: np.ndarray
    vert: np.ndarray


@dataclasses.dataclass
class Function:
    """A pair of arrays that describe a function over time."""

    times: np.ndarray
    values: np.ndarray

    @t.overload
    def __call__(self, time: float) -> float:
        ...

    @t.overload
    def __call__(self, time: np.ndarray) -> np.ndarray:
        ...

    def __call__(self, time: t.Union[float, np.ndarray]) -> t.Union[float, np.ndarray]:
        """Evaluate the function at the given time."""
        return np.interp(time, self.times, self.values)


def rms(
    array: np.ndarray,
    axis: t.Optional[int] = None,
    keepdims: bool = False,
) -> t.Union[float, np.floating, np.ndarray]:
    """Calculate the root-mean-square of an array."""
    square = np.square(array)
    mean = np.mean(square, axis=axis, keepdims=keepdims)
    return np.sqrt(mean)


def truncated_mean(
    array: t.Union[np.ndarray, t.Sequence[float]],
    discard: int,
    axis: t.Optional[int] = None,
    keepdims: bool = False,
) -> float:
    """Calculate the truncated mean of a number of values.

    This discards the `discard` largest elements and the `discard`
    smallest elements and then calculates the arithmetic mean.
    """
    arranged = np.sort(array, axis=axis)
    truncated = arranged[discard:-discard]
    return np.mean(truncated, axis=axis, keepdims=keepdims)


def clip_to_box(array: np.ndarray, space: Box) -> np.ndarray:
    """Clip an array so that the result lies within the given box."""
    return t.cast(np.ndarray, np.clip(array, space.low, space.high))


class BoxScaler:
    """Utility class to convert between two box spaces.

    Example:

        >>> scaler = BoxScaler(
        ...     source=Box(
        ...         low=np.array([-1.0, -1.0]),
        ...         high=np.array([1.0, 1.0]),
        ...         dtype=float,
        ...     ),
        ...     dest=Box(
        ...         low=np.array([0.0, 1.0]),
        ...         high=np.array([1.0, 10.0]),
        ...         dtype=float,
        ...     ),
        ... )
        >>> np.array_equal(scaler.to_dest(scaler.source.low), scaler.dest.low)
        True
        >>> np.array_equal(scaler.to_source(scaler.dest.high), scaler.source.high)
        True
        >>> scaler.to_dest([0.0, 0.0])
        array([0.5, 5.5])
    """

    def __init__(self, source: Box, dest: Box) -> None:
        if not (_is_bounded(source) and _is_bounded(dest)):
            raise ValueError("cannot transform unbounded boxes")
        self.source = source
        self.dest = dest

    def to_dest(self, value: np.ndarray, copy: bool = True) -> np.ndarray:
        """Convert a point in the source box into the dest box."""
        value = np.array(value, copy=copy)
        value -= self.source.low
        value *= self.scale
        value += self.dest.low
        return value

    def to_source(self, value: np.ndarray, copy: bool = True) -> np.ndarray:
        """Convert a point in the dest box into the source box."""
        value = np.array(value, copy=copy)
        value -= self.dest.low
        value /= self.scale
        value += self.source.low
        return value

    @property
    def scale(self) -> np.ndarray:
        """The linear scaling factor applied during transformation."""
        dest_range = self.dest.high - self.dest.low
        source_range = self.source.high - self.source.low
        return dest_range / source_range


def _is_bounded(box: Box) -> bool:
    return bool(np.all(np.logical_and(box.bounded_above, box.bounded_below)))

