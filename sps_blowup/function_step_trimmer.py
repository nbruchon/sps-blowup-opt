#!/usr/bin/env python

import typing as t

import numpy as np

import logging

from cern.lsa.domain.settings import ContextSettingsRequest, Settings
from cern.lsa.domain.settings import TrimRequest
from cern.lsa.client import (ContextService, ParameterService, ServiceLocator, SettingService, TrimService)
from cern.accsoft.commons.value import ValueFactory

if t.TYPE_CHECKING:
    import cern

logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger(__name__)


class NotFound(Exception):
    """The parameter, user or context was not found in the database."""


class FunctionStepTrimmer:
    """Trim a function over a number of steps within a given interval.

    Args:
        parameters: The names of the parameters to be trimmed.
        start: The start of the interval in which the parameter
            functions are trimmed.
        stop: The end of the interval in which the parameter functions
            are trimmed.
        num_steps: The number of steps into which to split the interval
            ``[start; stop]``.
        rolloff: The length of the interval in which each step ramps up
            and down. This is necessary because we cannot set true step
            functions to the database.
        context: The context in which to trim the parameters. If *user*
            is passed, this must be None.
        user: The user for which to trim the parameters. If *context* is
            passed, this must be None.

    This takes a number of function-type *parameters* in the LSA
    database in a given *context* and selects an *interval* in it. (The
    context can also be determined by the *user* who has currently
    loaded it.)

    This interval is split into a number of *steps*. Each step has
    approximately the same length. Steps are separated from each other
    by a small interval of length *rolloff*.

    All times are measured as *cycle time*, i.e. as integer milliseconds
    measured from the start of the cycle.

        >>> parameters = [
        ...     "SA.BlowupGenerator/Frequency#lowerFrequency",
        ...     "SA.BlowupGenerator/Frequency#upperFrequency",
        ...     "SA.BlowupGenerator/Amplitude",
        ... ]
        >>> trimmer = FunctionStepTrimmer(
        ...     parameters, start=5000, stop=6000, num_steps=10,
        ...     user="SPS.USER.SFTPRO1",
        ... )

    This class allows raising or lowering each step individually. Each
    successful change is remembered so that sending the same values
    twice is idempotent.

        >>> # Changes are always zero initially.
        >>> trimmer.get_changes_at(0)
        array([0., 0., 0.])
        >>> # Any changes are added on top of the initial function.
        >>> trimmer.set_changes_at(0, [10.0, 10.0, -5.0])
        >>> # The initial function has changed,
        >>> # our changes are being remembered by the trimmer.
        >>> # Note that these are changes w.r.t. the actual function in LSA.
        >>> # We have never read its actual shape from the database!
        >>> trimmer.get_changes_at(0)
        array([10., 10., -5.])
        >>> # Sending the same values again won't change the function.
        >>> trimmer.set_changes_at(0, [10.0, 10.0, -5.0])
        >>> trimmer.get_changes_at(0)
        array([10., 10., -5.])
        >>> # Setting all changes to zero *should* recover the original shape.
        >>> trimmer.set_changes_at(0, [0, 0, 0])
        >>> trimmer.get_changes_at(0)
        array([0., 0., 0.])

    All changes made in one call to :meth:`set_changes_at()` call are
    sent as a single trim request; this means that if one of them fails,
    none are written to the database.
    """

    # pylint: disable = too-many-instance-attributes

    def __init__(
        self,
        parameters: t.List[str],
        start: int,
        stop: int,
        num_steps: int,
        *,
        rolloff: int = 100,
        context: t.Optional[str] = None,
        user: t.Optional[str] = None,
    ) -> None:
        self._services = Services()
        self._parameters = []
        for name in parameters:
            parameter = self._services.parameter.findParameterByName(name)
            if not parameter:
                raise NotFound(name)
            self._parameters.append(parameter)
        self._cycle = find_cycle(self._services, context=context, user=user)
        self._start = start
        self._stop = stop
        self._num_steps = num_steps
        self._rolloff = rolloff
        self._changes = np.zeros((self._num_steps, len(self._parameters)))
        assert 0 < num_steps <= stop - start
        assert 0 <= start < stop <= self.cycle_length

    @property
    def start(self) -> int:
        """The start of the interval in which this function is being trimmed."""
        return self._start

    @property
    def stop(self) -> int:
        """The end of the interval in which this function is being trimmed."""
        return self._stop

    @property
    def num_steps(self) -> int:
        """The number of steps into which the trim interval is split.

        The steps have approximately but not exactly the same length.
        The reason for that is that the function's X coordinates must be
        integers.
        """
        return self._num_steps

    @property
    def num_params(self) -> int:
        """The number of parameters that are trimmed with each request."""
        return len(self._parameters)

    @property
    def rolloff(self) -> int:
        """The length of the trapezoid rolloff of each step.

        Trims cannot send discrete step functions to LSA. Instead, we
        send trapezoidal almost-step functions. The rolloff is the
        length the interval within which the function linearly goes from
        zero to the value to be sent.
        """
        return self._rolloff

    @property
    def parameters(self) -> t.Tuple[str, ...]:
        """The names of the parameters to trim."""
        return tuple(p.getName() for p in self._parameters)

    @property
    def cycle_length(self) -> int:
        """The length of the cycle in milliseconds."""
        return self._cycle.getLength()

    def request_functions(self) -> t.List[t.Tuple[np.ndarray, np.ndarray]]:
        """Query the functions of each parameter from the database.

        This returns the function as a 2-tuple of times and values, each
        an 1D array of equal length.

        Example:

            >>> from matplotlib import pyplot
            >>> trimmer = FunctionStepTrimmer(...)
            >>> functions = trimmer.request_functions()
            >>> for name, func in zip(trimmer.parameters, functions):
            ...     [xs, ys] = func
            ...     pyplot.plot(xs, ys, label=name)
            >>> pyplot.legend()
            >>> pyplot.show()
        """
        # pylint: disable = import-outside-toplevel
        # from cern.lsa.domain.settings import ContextSettingsRequest, Settings

        builder = ContextSettingsRequest.builder()
        request = builder.byStandAloneContextAndParameters(
            self._cycle, self._parameters
        )
        settings = self._services.setting.findContextSettings(request)
        functions = (Settings.getFunction(settings, p) for p in self._parameters)
        return [(np.array(f.toXArray()), np.array(f.toYArray())) for f in functions]

    def get_changes_at(self, step: int) -> np.ndarray:
        """Return the changes last sent to the database for the given step.

        If no changes have been sent yet, this returns zeros.

        Args:
            step: The index of the step. Must be at least 0 and less
                than `self.num_steps`.

        Returns:
            A 1-D array of length `self.num_params` with the sent change
            for each parameter.
        """
        return self._changes[step, :].copy()

    def set_changes_at(
        self, step: int, values: np.ndarray, description: t.Optional[str] = None
    ) -> None:
        """Send changes to the database for the current step.

        The changes are added on top of the function, however its shape
        in the LSA database. We do this without querying the function.

        Each successful change is remembered. Sending the same values
        twice does nothing. Sending all zeros should return the function
        to its original shape.

        This means that if an independent party performs trims while
        this `FunctionStepTrimmer` is running, the trimmer will likely
        get confused!

        Args:
            step: The index of the step. Must be at least 0 and less
                than `self.num_steps`.
            values: The changes to be sent, one per parameter.
            description: An optional comment that appears in the LSA
                trim history.
        """
        if np.shape(values) != (len(self._parameters),):
            raise ValueError(
                f"expected shape {(len(self._parameters),)}, got {np.shape(values)}"
            )
        prev_values = self.get_changes_at(step)
        if np.shape(prev_values) != np.shape(values):
            raise ValueError(
                f"expected shape {np.shape(values)}, got {np.shape(prev_values)}"
            )
        request = self._build_trim_request(
            step,
            trim_values=values - prev_values,
            relative=True,
            description=description,
        )
        self._services.trim.trimSettings(request)
        # Only update changes if `trimSettings()` doesn't raise an
        # exception.
        self._changes[step, :] = values

    def _build_trim_request(
        self,
        step: int,
        trim_values: np.ndarray,
        *,
        relative: bool,
        description: t.Optional[str],
    ) -> "cern.lsa.domain.settings.TrimRequest":
        """Build a trim request ready to be submitted to LSA.

        This creates one trapezoid function for each parameter.

        Args:
            step: The step to be manipulated.
            trim_values: The non-zero values of the trapezoid functions,
                one for each parameter.
            relative: If True, add the trapezoid function on top of the
                existing function in LSA. If False, replace the function
                in LSA with the trapezoid function.
            description: An optional comment that appears in the LSA
                trim history.

        Return:
            The complete trim request.
        """
        # pylint: disable = import-outside-toplevel
        # from cern.lsa.domain.settings import TrimRequest

        assert np.shape(trim_values) == (len(self._parameters),)
        cycle_length = self.cycle_length
        rolloff = self.rolloff
        step_start = self._get_step_start(step)
        step_stop = self._get_step_stop(step)
        builder = TrimRequest.builder()
        builder.setContext(self._cycle)
        builder.setRelative(relative)
        if description is not None:
            builder.setDescription(description)
        for parameter, trim_value in zip(self._parameters, trim_values):
            function = create_trapezoid_function(
                length=cycle_length,
                start=step_start,
                stop=step_stop,
                height=trim_value,
                rolloff=rolloff,
            )
            builder.addFunction(parameter, function)
        return builder.build()

    def _get_step_start(self, step: int) -> int:
        """Get the start of the given step.

        The start is defined to be the point where the trapezoidal
        change reaches the beginning of its plateau. The end is the
        ending of the plateau. From both points, the function goes to
        zero over a period known as the *rolloff*.

        Because of the rolloff, the end of one step does not coincide
        with the start of the next step. They're exactly `self.rolloff`
        apart.

        The start of the first step coincides with `self.start` and the
        end of the last step with `self.stop`. This means that the first
        step's left rolloff and the last step's right rolloff extend
        beyond the given interval. This is a bit spooky, but intuitive
        behavior.
        """
        # Order operations such that the loss of precision is minimal.
        interval_length = self.stop - self.start
        offset = step * (interval_length + self.rolloff) // self.num_steps
        return self.start + offset

    def _get_step_stop(self, step: int) -> int:
        """Get the stop of the given step.

        This is exactly `self.rolloff` away from the start of the next
        step.
        """
        return self._get_step_start(step + 1) - self.rolloff


class Services:
    """Simple holder class to allow lazy-loading the LSA services."""

    # pylint: disable = too-few-public-methods

    trim: "cern.lsa.client.common.CommonTrimService"
    parameter: "cern.lsa.client.common.CommonParameterService"
    context: "cern.lsa.client.common.CommonContextService"
    setting: "cern.lsa.client.common.CommonSettingService"

    def __init__(self) -> None:
        # pylint: disable = import-outside-toplevel
        # from cern.lsa.client import (ContextService, ParameterService, ServiceLocator, SettingService, TrimService)

        self.trim = ServiceLocator.getService(TrimService)
        self.parameter = ServiceLocator.getService(ParameterService)
        self.context = ServiceLocator.getService(ContextService)
        self.setting = ServiceLocator.getService(SettingService)


def find_cycle(
    services: Services, *, context: t.Optional[str] = None, user: t.Optional[str] = None
) -> "cern.lsa.domain.settings.StandAloneCycle":
    """Look up a stand-alone cycle by name or user."""
    # pylint: disable = import-outside-toplevel
    if context and user:
        raise TypeError("conflicting arguments: 'context' and 'user'")
    if user:
        try:
            cycle = services.context.findStandAloneContextByUser(user)
        except Exception:
            raise NotFound(user) from None
        if not cycle.isStandAlone():
            raise TypeError(f"not a stand-alone cycle: {cycle!s}")
        return t.cast("cern.lsa.domain.settings.StandAloneCycle", cycle)
    if context:
        cycle = services.context.findStandAloneCycle(context)
        if not cycle:
            raise NotFound(context)
        return cycle
    raise TypeError("missing keyword-only argument: 'context' or 'user'")


def create_trapezoid_function(
    *,
    length: int,
    height: int,
    start: int,
    stop: int,
    rolloff: int,
) -> "cern.accsoft.commons.value.DiscreteFunction":
    """Create a trapezoid function suitable for trimming.

    The function is defined in an interval ``[0; length]``. It is zero
    everywhere except in an interval ``[start-rolloff; stop+rolloff]``.
    It has a value of ``height`` on an interval ``[start; stop]``.
    Between the two values, it changes linearly over a span of length
    ``rolloff`.
    """
    # from cern.accsoft.commons.value import ValueFactory

    assert 0 < rolloff
    assert 0 <= start - rolloff < start <= stop < stop + rolloff <= length
    return ValueFactory.createFunction(
        np.array([start - rolloff, start, stop, stop + rolloff], dtype=float),
        np.array([0.0, height, height, 0.0], dtype=float),
    )
