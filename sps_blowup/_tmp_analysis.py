# Automated Optimization of Controlled Longitudinal Emittance Blow-Up
# Developed by N. Bruchon in 2022

# import
import logging
import typing as t
import numpy as np


class Analysis:

    @staticmethod
    def get_n_bunches(waveform: np.ndarray, cycle_time: float) -> tuple:
        """

        """
        fit_window = 65  # for LHC beams the fit window is 10 ns

        peak_val = []
        peak_idx = []

        # Peaks search
        amp_max = np.max(waveform)
        tmp_peak_idx = 0  # Peak index
        tmp_peak_val = 0  # Peak index
        bunch_cnt = 0

        for i in range(len(waveform)):
            if waveform[i] > 0.3 * amp_max:
                if waveform[i] > tmp_peak_val:
                    tmp_peak_val = waveform[i]
                    tmp_peak_idx = i

            if (tmp_peak_val != 0) and (i + 1 > tmp_peak_idx + 0.75 * fit_window):
                peak_val.append(tmp_peak_val)
                peak_idx.append(tmp_peak_idx)

                bunch_cnt += 1

                tmp_peak_idx = 0
                tmp_peak_val = 0

        n_bunches = bunch_cnt
        bunch_position = np.array(peak_idx)

        return cycle_time, n_bunches, waveform, bunch_position

    @staticmethod
    def get_bunches(
            n_bunches: int,
            waveform: np.ndarray,
            bunch_position: np.ndarray,
            n_samples: int = 65) -> np.ndarray:
        """

        """
        bunches = []
        half_samples = int(np.floor(n_samples / 2))

        for n_bunch in range(n_bunches):
            bunch_center = bunch_position[n_bunch]
            bunch_center_idx = int(np.round(bunch_center))
            bunch = waveform[bunch_center_idx - half_samples: bunch_center_idx + half_samples + 1]

            # Define offset level
            time_off = int(0.2 * half_samples - 1)
            time_off2 = np.where(
                bunch > np.min(bunch[0:int(np.floor(len(bunch) / 2))]) +
                .1 * (np.max(bunch) - np.min(bunch))
            )[0][0]
            time_off = np.floor(min(time_off, time_off2))

            offset_level = np.mean(bunch[0:int(time_off) + 1])
            bunch = bunch - offset_level

            bunches.append(bunch)

        return np.array(bunches)

    @classmethod
    def get_width(cls, timescale: np.ndarray, bunch: np.ndarray, level: float) -> tuple:
        """

        """
        t1, t2 = cls.interp_f(timescale, bunch, level)
        width = t2 - t1
        center = (t1 + t2) / 2
        sigma = width / (2 * np.sqrt(-2 * np.log(level)))
        bunch_length = 4 * sigma  # SPS standard

        return width, center, sigma, bunch_length

    @classmethod
    def interp_f(cls, timescale: np.ndarray, bunch: np.ndarray, level: float) -> tuple:
        """Interpolation method (for FWHM)"""

        tau_x = np.where(bunch > level * np.max(bunch))[0][0]
        t1 = timescale[tau_x] - (bunch[tau_x] - level * np.max(bunch)) \
            / (bunch[tau_x] - bunch[tau_x - 1]) * (timescale[tau_x] - timescale[tau_x - 1])

        tau_x = np.where(bunch > level * np.max(bunch))[0][-1]
        t2 = timescale[tau_x] + (bunch[tau_x] - level * np.max(bunch)) \
            / (bunch[tau_x] - bunch[tau_x + 1]) * (timescale[tau_x + 1] - timescale[tau_x])

        return t1, t2
