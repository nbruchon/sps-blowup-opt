# Automated Optimization of Controlled Longitudinal Emittance Blow-Up
# Developed by N. Bruchon in 2022

#############
# IMPORTING #
#############
import logging
# from pathlib import Path
import typing as t

import numpy as np
from cernml.coi import cancellation

from gym.spaces import Box
from cernml import gym_utils

from ._utils import CycleTime

# TODO: rename the following files
from ._tmp_real_profiles import RealProfiles
from ._tmp_sim_profiles import SimulatedProfiles
from ._tmp_analysis import Analysis

from .function_step_trimmer import FunctionStepTrimmer

import java.util
import jpype
from cern.lsa.client import ContextService, ParameterService, ServiceLocator, SettingService, TrimService
from cern.lsa.client.common import CommonContextService, CommonSettingService
from cern.accsoft.commons.value import ValueFactory
from cern.lsa.domain.settings import ContextSettingsRequest, Settings, TrimRequest

if t.TYPE_CHECKING:
    from cernml import lsa_utils
    from pyjapc import PyJapc

logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger(__name__)


#########
# CLASS #
#########
class Communicator:
    """
        Class to encapsulate machine or simulated communications.

        Keyword args:
            simulation: boolean, define simulator (True) or SPS (False)
    """

    ######################
    # LSA PARAMETER TYPE #
    ######################
    _parameters: t.ClassVar[t.Tuple[str, ...]] = (
        'SA.BlowUpGenerator/Amplitude#amplitude',
        'SpsLowLevelRF/LongitudinalBlowUp#margin_low',
        'SpsLowLevelRF/LongitudinalBlowUp#margin_high'
    )

    #####################################
    # BLOW-UP NOISE GENERATION ENABLING #
    #####################################
    BLOWUP_GENERATOR: t.ClassVar[str] = "SA.BlowUpGenerator/Settings#enabled"

    ##########################
    # DESIRED BEAM STRUCTURE #
    ##########################
    FILLING_PATTERN: t.ClassVar[str] = "BeamCtrl/FillingPattern#fillingPattern"

    ###################
    # RF BEAM CONTROL #
    ###################
    TARGET_BUNCH_LENGTH: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#targetBunchLength"
    START_TIME: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#startTime"
    STOP_TIME: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#stopTime"
    N_SUBINTERVALS: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#nSubintervalls"
    N_BUNCHES_REQUESTED: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#nBunchesRequested"

    #    # AMPLITUDE
    AMPLITUDE_ENABLE: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#amplitudeEnable"
    AMPLITUDE_MAX: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#amplitudeMax"
    AMPLITUDE_MIN: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#amplitudeMin"
    #    # MARGIN LOW
    MARGIN_LOW_ENABLE: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#marginLowEnable"
    MARGIN_LOW_MAX: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#marginLowMax"
    MARGIN_LOW_MIN: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#marginLowMin"
    #    # MARGIN HIGH
    MARGIN_HIGH_ENABLE: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#marginHighEnable"
    MARGIN_HIGH_MAX: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#marginHighMax"
    MARGIN_HIGH_MIN: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#marginHighMin"

    ##########################
    # RF BEAM CONTROL EXPERT #
    ##########################
    COST_FUNCTION: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#costFunction"
    N_BUNCHES_DISCARD: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#nBunchesDiscard"
    TARGET_TOLERANCE: t.ClassVar[str] = "SpsLowLevelRF/LongitudinalBlowUp#targetTolerance"

    ###############################
    # LSA BACKWARD RULE PARAMETER #
    ###############################
    TRIANGLEIR: int = 100
    _rolloff: int = 100

    ##########
    # OTHERS #
    ##########
    _savedata: bool = False
    _nbunches: int
    _min_margins_distance: float = 0.05

    _current_params: np.ndarray
    _current_params_to_opt: np.ndarray
    _current_params_norm: np.ndarray
    _current_params_to_opt_norm: np.ndarray

    ###############
    # CONSTRUCTOR #
    ###############
    def __init__(
            self,
            japc: "PyJapc",
            token: cancellation.Token,
            simulation: bool,
    ) -> None:
        """Communicator class initialization"""

        self._japc = japc
        self._token = token
        self._simulation = simulation

        ################################
        # CONNECTION TO LSA PARAMETERS #
        ################################
        client = jpype.JPackage("cern.lsa.client")
        self._lsa_trim_service = client.ServiceLocator.getService(client.TrimService)
        self._lsa_parameter_service = client.ServiceLocator.getService(client.ParameterService)
        self.context_service = client.ServiceLocator.getService(client.ContextService)

        parameter_service = ServiceLocator.getService(ParameterService)

        # BLOW-UP NOISE GENERATOR
        self._blowup_generator = parameter_service.findParameterByName(self.BLOWUP_GENERATOR)
        # FILLING PATTERN
        self._filling_pattern = parameter_service.findParameterByName(self.FILLING_PATTERN)

        ###################
        # RF BEAM CONTROL #
        ###################
        self._target_bunch_length = parameter_service.findParameterByName(self.TARGET_BUNCH_LENGTH)
        self._start_time = parameter_service.findParameterByName(self.START_TIME)
        self._stop_time = parameter_service.findParameterByName(self.STOP_TIME)
        self._n_subintevals = parameter_service.findParameterByName(self.N_SUBINTERVALS)
        self._n_bunches_requested = parameter_service.findParameterByName(self.N_BUNCHES_REQUESTED)

        # AMPLITUDE
        self._amplitude_enable = parameter_service.findParameterByName(self.AMPLITUDE_ENABLE)
        self._amplitude_min = parameter_service.findParameterByName(self.AMPLITUDE_MIN)
        self._amplitude_max = parameter_service.findParameterByName(self.AMPLITUDE_MAX)
        # MARGIN LOW
        self._margin_low_enable = parameter_service.findParameterByName(self.MARGIN_LOW_ENABLE)
        self._margin_low_min = parameter_service.findParameterByName(self.MARGIN_LOW_MIN)
        self._margin_low_max = parameter_service.findParameterByName(self.MARGIN_LOW_MAX)
        # MARGIN HIGH
        self._margin_high_enable = parameter_service.findParameterByName(self.MARGIN_HIGH_ENABLE)
        self._margin_high_min = parameter_service.findParameterByName(self.MARGIN_HIGH_MIN)
        self._margin_high_max = parameter_service.findParameterByName(self.MARGIN_HIGH_MAX)

        ##########################
        # RF BEAM CONTROL EXPERT #
        ##########################
        self._cost_function = parameter_service.findParameterByName(self.COST_FUNCTION)
        self._n_bunches_discard = parameter_service.findParameterByName(self.N_BUNCHES_DISCARD)
        self._target_tolerance = parameter_service.findParameterByName(self.TARGET_TOLERANCE)

        ###################
        # CONNECT TO JAVA #
        ###################
        self._user = japc.getSelector()
        context: CommonContextService = ServiceLocator.getService(ContextService)
        self.setting: CommonSettingService = ServiceLocator.getService(SettingService)
        self._cycle = context.findStandAloneContextByUser(self.user)

        ################
        # TRIM SERVICE #
        ################
        self.trimService = ServiceLocator.getService(TrimService)

        # PRINT LSA PARAMETERS IN LOG
        LOG.info(f"Report of LSA parameters:\n"
                 f"\n"
                 f"#########################################################\n"
                 f"USER:    {self.user}                                     \n"
                 f"CYCLE:   {self.cycle}                                    \n"
                 f"---------------------------------------------------------\n"
                 f"Simulation:              {self.simulation}               \n"
                 f"Cost function:           {self.cost_function}            \n"
                 f"Target bunch length:     {self.target_bunch_length}      \n"
                 f"Target tolerance:        {self.target_tolerance}         \n"
                 f"Num. bunches expected:   {self.n_bunches_requested}      \n"
                 f"Num. bunches to discard: {self.n_bunches_discard}        \n"
                 f"---------------------------------------------------------\n"
                 f"Start time:          {self.start_time}                   \n"
                 f"Stop time:           {self.stop_time}                    \n"
                 f"Num. subintervals:   {self.n_subintervals}               \n"
                 f"---------------------------------------------------------\n"
                 f"AMPLITUDE:   {self.amplitude_enable}, "
                 f"range = ({self.amplitude_min}, {self.amplitude_max})     \n"
                 f"MARGIN LOW:  {self.margin_low_enable}, "
                 f"range = ({self.margin_low_min}, {self.margin_low_max})   \n"
                 f"MARGIN_HIGH: {self.margin_high_enable}, "
                 f"range = ({self.margin_high_min}, {self.margin_high_max}) \n"
                 f"---------------------------------------------------------\n"
                 f"Generator status: {self.noise_generator_status}          \n"
                 f"#########################################################\n")

        #######################
        # INITIAL DEFINITIONS #
        #######################
        # indexes = np.arange(self.num_parameters)  # np.ndarray = [0, 1, 2]
        self._to_optimize = np.array(self.opt_parameters)  # np.ndarray of booleans

        # Parameter addresses
        params_addr = np.array(self.parameters)  # np.ndarray of addresses (amp, margin low, margin high)

        # Parameter names
        self._params_name = np.array(self.parameter_names)  # np.ndarray ('Amplitude', 'MarginLow', 'MarginHigh')
        self._params_to_opt_name = self._params_name[self.to_optimize]

        # Parameter incorporators
        self.params_incorporator = self._make_params_incorporators(
            params=list(params_addr)  # sostituire con self.parameters?
        )

        # Parameter ranges
        min_params = np.array(self.min_parameters)
        max_params = np.array(self.max_parameters)
        self.params_box = Box(
            low=min_params,
            high=max_params,
            dtype=np.float64
        )

        # Parameter scalers
        self.params_scaler = gym_utils.Scaler(
            self.params_box, symmetric=True
        )

        # Frequency margin box
        self.margins_box = Box(
            low=np.array(
                [self.margin_low_min, self.margin_high_min]
            ),
            high=np.array(
                [self.margin_low_max, self.margin_high_max]
            ),
            dtype=np.float64
        )

        # Parameters to optimize
        self._parameters_to_opt = tuple(np.array(self._parameters)[self.to_optimize])

        # Parameter to optimize ranges
        min_params_to_opt = min_params[self.to_optimize]
        max_params_to_opt = max_params[self.to_optimize]
        self.params_to_opt_box = Box(
            low=min_params_to_opt,
            high=max_params_to_opt,
            dtype=np.float64
        )

        # Parameter to optimize incorporators
        self.params_to_opt_incorporator = tuple(
            np.array(self.params_incorporator)[self.to_optimize]
        )

        # Parameter to optimize scaler
        self.params_to_opt_scaler = gym_utils.Scaler(
            self.params_to_opt_box, symmetric=True
        )

        #####################
        # INITIAL SITUATION #
        #####################
        self._initial_params = np.array([self.get_params(skp) for skp in self.skeleton_points])
        self.initial_params = self._initial_params

        self._initial_params_to_set = self._initial_params[:, self.to_optimize]
        self.initial_params_to_set = self._initial_params_to_set

        ########################
        # PROFILES ACQUISITION #
        ########################
        _observation_points = tuple(self._get_step_stop(step) for step in range(self.n_subintervals))
        if self.simulation:
            self.meas = SimulatedProfiles(
                observation_points=np.array(_observation_points) - self.first_injection_time,
            )
            self.dtime = 3.90e-11
        else:
            self.meas = RealProfiles(
                injection=self.last_injection_time,
                extraction=self.extraction_time,
                nbunches=self.n_bunches_requested,
                observation_points=np.array(_observation_points) - self.first_injection_time,
                japc=self.japc,
                token=self.token)
            self.dtime = 0.125e-9

        #####################
        # PROFILES ANALYSIS #
        #####################
        self.an = Analysis()

        ###########################
        # TRIMM SUB-INTERVAL ONLY #
        ###########################
        self.trimmer = FunctionStepTrimmer(
            parameters=list(self.parameters_enable),  # self.parameters),
            start=int(self.start_time),
            stop=int(self.stop_time - self.rolloff),
            num_steps=self.n_subintervals,
            rolloff=int(self.rolloff),
            user=self.user
        )

    def _get_all_params(self) -> np.ndarray:
        """ """
        all_params = [self.get_params(skp) for skp in self.skeleton_points]
        return np.array(all_params)

    @property
    def savedata(self) -> bool:
        return self._savedata

    # TODO: isolate properties whose change during optimization may affect the process,
    #  i.e. LSA parameters that may be manually changed during blow-up optimization...
    #  Values used only in _baseenv.py are managed within the BaseEnv class...
    # - cost_function
    # - n_bunches_requested
    # - n_subintervals
    # - amplitude_enable
    # - amplitude_min
    # - amplitude_max
    # - margin_low_enable
    # - margin_low_min
    # - margin_low_max
    # - margin_high_enable
    # - margin_high_min
    # - margin_high_max
    # - times...

    @property
    def noise_generator_status(self) -> bool:
        """ """
        request_1 = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self.blowup_noise_generator)
        )
        settings_1 = self.setting.findContextSettings(request_1)
        function_1 = Settings.getScalar(settings_1, self.blowup_noise_generator)
        return function_1.getBoolean()

    @property
    def cost_function_items(self) -> list:
        """ """
        descriptor = self._cost_function.getValueDescriptor()
        objects = list(descriptor.getEnumType().values())
        return [obj.getSymbol() for obj in objects]

    @property
    def cost_function(self) -> str:
        """ """
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._cost_function)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._cost_function)
        return function.getString()

    @property
    def n_bunches_requested(self) -> int:
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._n_bunches_requested)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._n_bunches_requested)
        return function.getInt()

    @property
    def n_bunches_discard(self) -> int:
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._n_bunches_discard)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._n_bunches_discard)
        return function.getInt()

    @property
    def target_tolerance(self) -> float:
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._target_tolerance)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._target_tolerance)
        return round(function.getFloat(), ndigits=3)

    @property
    def target_bunch_length(self) -> float:
        """ """
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._target_bunch_length)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._target_bunch_length)
        return function.getDouble()

    @property
    def start_time_injection(self) -> int:
        """ """
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._start_time)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._start_time)
        return function.getInt()

    @property
    def start_time(self) -> int:
        """ """
        return int(self.start_time_injection + self.first_injection_time)

    @property
    def stop_time_injection(self) -> int:
        """ """
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._stop_time)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._stop_time)
        return function.getInt()

    @property
    def stop_time(self) -> int:
        """ """
        return int(self.stop_time_injection + self.first_injection_time)

    @property
    def n_subintervals(self) -> int:
        """ """
        # TODO: check to ensure it is > 0
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._n_subintevals)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._n_subintevals)
        return function.getInt()

    @property
    def amplitude_enable(self) -> bool:
        """ """
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._amplitude_enable)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._amplitude_enable)
        return function.getBoolean()

    @property
    def amplitude_min(self) -> float:
        """ """
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._amplitude_min)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._amplitude_min)
        return round(function.getFloat(), ndigits=3)

    @property
    def amplitude_max(self) -> float:
        """ """
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._amplitude_max)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._amplitude_max)
        return round(function.getFloat(), ndigits=3)

    @property
    def margin_low_enable(self) -> bool:
        """ """
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._margin_low_enable)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._margin_low_enable)
        return function.getBoolean()

    @property
    def margin_low_min(self) -> float:
        """ """
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._margin_low_min)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._margin_low_min)
        return round(function.getFloat(), ndigits=3)

    @property
    def margin_low_max(self) -> float:
        """ """
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._margin_low_max)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._margin_low_max)
        return round(function.getFloat(), ndigits=3)

    @property
    def margin_high_enable(self) -> bool:
        """ """
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._margin_high_enable)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._margin_high_enable)
        return function.getBoolean()

    @property
    def margin_high_min(self) -> float:
        """ """
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._margin_high_min)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._margin_high_min)
        return round(function.getFloat(), ndigits=3)

    @property
    def margin_high_max(self) -> float:
        """ """
        request = ContextSettingsRequest.byStandAloneContextAndParameters(
            self.cycle, java.util.Collections.singleton(self._margin_high_max)
        )
        settings = self.setting.findContextSettings(request)
        function = Settings.getScalar(settings, self._margin_high_max)
        return round(function.getFloat(), ndigits=3)

    @property
    def japc(self):
        """ """
        return self._japc

    @property
    def token(self):
        """ """
        return self._token

    @property
    def user(self):
        """ """
        return self._user

    @property
    def cycle(self):
        """ """
        return self._cycle

    @property
    def simulation(self) -> bool:
        """ """
        return self._simulation

    @property
    def min_margins_distance(self) -> float:
        """ """
        return self._min_margins_distance

    @property
    def to_optimize(self) -> np.ndarray:
        """ """
        return self._to_optimize

    @property
    def rolloff(self) -> int:
        """The length of the trapezoid rolloff of each step."""
        return self._rolloff

    @property
    def opt_parameters(self) -> t.Tuple[bool, ...]:
        """ """
        return self.amplitude_enable, self.margin_low_enable, self.margin_high_enable

    @property
    def min_parameters(self) -> t.Tuple[float, ...]:
        """ """
        return self.amplitude_min, self.margin_low_min, self.margin_high_min

    @property
    def max_parameters(self) -> t.Tuple[float, ...]:
        """ """
        return self.amplitude_max, self.margin_low_max, self.margin_high_max

    @property
    def parameters(self) -> t.Tuple[str, ...]:
        """ """
        return self._parameters

    @property
    def parameters_enable(self) -> np.ndarray:
        """ """
        return np.array(self.parameters)[list(self.opt_parameters)]

    @property
    def blowup_noise_generator(self):
        """ """
        return self._blowup_generator

    @property
    def filling_pattern(self):
        """ """
        return self._filling_pattern

    @property
    def skeleton_points(self) -> t.Tuple[int, ...]:
        """Skeleton points in cycle time"""
        return tuple(self._get_step_start(step) for step in range(self.n_subintervals))

    @property
    def observation_points(self) -> t.Tuple[int, ...]:
        """Observation points in injection time"""
        return self.meas.injection_based_times

    @property
    def params_name(self) -> t.Tuple[str, ...]:
        """ """
        return tuple(self._params_name)

    @property
    def params_to_opt_name(self) -> t.Tuple[str, ...]:
        """ """
        return tuple(self._params_to_opt_name)

    @property
    def num_parameters_to_opt(self) -> int:
        """The number of parameters."""
        return len(self._parameters_to_opt)

    @property
    def context(self) -> str:
        """Cycle context"""
        return self.cycle.toString()

    @property
    def all_bunchlengths(self) -> t.Tuple[list, ...]:
        """ """
        return self.meas._all_bunchlengths

    @property
    def first_injection_time(self) -> CycleTime:
        """Read beam injection time from LSA"""
        from cernml import lsa_utils
        attributes = lsa_utils.get_cycle_type_attributes(self.context)
        injection_time = attributes["VE:First injection"]
        return CycleTime(float(injection_time))

    @property
    def last_injection_time(self) -> CycleTime:
        """Read beam injection time from LSA"""
        from cernml import lsa_utils
        attributes = lsa_utils.get_cycle_type_attributes(self.context)
        last_injection_time = attributes["lastInjectionTimeMs"]
        return CycleTime(float(last_injection_time))

    @property
    def extraction_time(self) -> CycleTime:
        """Extraction time"""
        from cernml import lsa_utils
        attributes = lsa_utils.get_cycle_type_attributes(self.context)
        extraction_time = attributes["VE:General acquisition"]
        return CycleTime(float(extraction_time))

    @property
    def ramp_start(self) -> CycleTime:
        from cernml import lsa_utils
        attributes = lsa_utils.get_cycle_type_attributes(self.context)
        ramp_start = attributes["VE:Start ramp 1"]
        return CycleTime(float(ramp_start))

    @property
    def ramp_start_injection(self) -> CycleTime:
        return CycleTime(self.ramp_start - self.first_injection_time)

    @property
    def beam_out(self) -> CycleTime:
        from cernml import lsa_utils
        attributes = lsa_utils.get_cycle_type_attributes(self.context)
        beam_out = attributes["VE:Beam out"]
        return CycleTime(float(beam_out))

    @property
    def beam_out_injection(self) -> CycleTime:
        return CycleTime(self.beam_out - self.first_injection_time)

    @property
    def flat_top_start(self) -> CycleTime:
        """Flat top time"""
        from cernml import lsa_utils
        attributes = lsa_utils.get_cycle_type_attributes(self.context)
        flattop_time = attributes["VE:Start flat top"]
        return CycleTime(float(flattop_time))

    @property
    def flat_top_start_injection(self) -> CycleTime:
        return CycleTime(self.flat_top_start - self.first_injection_time)

    @property
    def current_params(self) -> np.ndarray:
        """ """
        return self._current_params

    @property
    def current_params_to_opt(self) -> np.ndarray:
        """ """
        return self._current_params_to_opt

    @property
    def current_params_norm(self) -> np.ndarray:
        """ """
        return self._current_params_norm

    @property
    def current_params_to_opt_norm(self) -> np.ndarray:
        """ """
        return self._current_params_to_opt_norm

    def _get_step_start(self, step: int) -> int:
        """Get the start of the given step."""
        interval_length = self.stop_time - self.rolloff - self.start_time
        offset = step * (interval_length + self.rolloff) // self.n_subintervals
        return self.start_time + offset

    def _get_step_stop(self, step: int) -> int:
        """Get the stop of the given step."""
        return self._get_step_start(step + 1) - int(self.rolloff/10)

    ###################
    # GENERAL METHODS #
    ###################

    def close(self) -> None:
        """
            Perform any necessary cleanup.
        """
        if not self.simulation:
            self.meas._close()

        return None

    def reset(self, cycle_time) -> None:
        """
            Reset settings to random value within the box.
        """
        # values = self.params_to_opt_box.sample()
        # values = self.margin_constrain(values, self.params_to_opt_name)
        # values = self.clip_to_space(values, self.params_to_opt_box)
        # self.set_params(cycle_time=cycle_time, values=values)

    @property
    def parameter_names(self) -> t.Tuple[str, ...]:
        """
            Collect all params name in a tuple
            Returns: tuple with all params names
        """

        def _extract_param_name(addr: str) -> str:
            dev, _, prop = addr.partition("/")
            attr, _, field = prop.partition("#")
            return f"{field}"

        return tuple(_extract_param_name(addr) for addr in self.parameters)

    def _make_params_incorporators(self, params: list) -> tuple:  # t.Tuple[lsa_utils.Incorporator, ...]:
        """
            Pncorporate the parameters provided.
            Parameters:
                params - all params addresses.
            Returns:
                params_incorporators - tuple of all params incorporators
        """
        from cernml import lsa_utils
        return tuple(
            lsa_utils.Incorporator(parameter=param, user=self.user) for param in params
        )

    #####################
    # GET PARAM METHODS #
    #####################
    def get_params_norm(self, cycle_time: float) -> np.ndarray:
        """
            Get normalized parameters values.
        """
        values = self.get_params(cycle_time)
        _current_params_norm = self.params_scaler.scale(values)
        self._current_params_norm = _current_params_norm
        self._current_params_to_opt_norm = _current_params_norm[self.to_optimize]
        return _current_params_norm

    def get_params(self, cycle_time: float) -> np.ndarray:
        """
            Get parameters values.
        """
        values = []
        for param in self.params_incorporator:
            x, y = self.get_param_function(param=param)
            value = np.interp(cycle_time, x, y)
            values.append(value)

        _current_params = np.round(values, decimals=4)
        self._current_params = _current_params
        self._current_params_to_opt = _current_params[self.to_optimize]

        return _current_params

    @staticmethod
    def get_param_function(param) -> np.ndarray:
        """
            Get parameters values from LSA function.
            Parameters:
                param - parameter incorporator.
            Returns:
                array - x and y values of the parameter function.
        """
        x, y = param.get_function()
        nan_check = np.isnan(y)
        if any(nan_check):
            nan_idxs = np.where(nan_check == True)[0]
            not_nan_idxs = np.where(nan_check == False)[0]
            for nan_idx in nan_idxs:
                idx = not_nan_idxs.flat[
                    np.abs(not_nan_idxs - nan_idx).argmin()
                ]
                y[nan_idx] = y[idx]
        return np.array([x, y])

    ######################
    # SET PARAMS METHODS #
    ######################

    # AMPLITUDE
    def set_amplitude(self, value: bool):

        scalar = ValueFactory.createScalar(value)

        trimRequest = TrimRequest.builder() \
            .setContext(self.cycle) \
            .addScalar(self._amplitude_enable, scalar) \
            .setDescription("COI trimming amplitudeEnable") \
            .build()

        self.trimService.trimSettings(trimRequest)

    def set_amplitude_min(self, value: float):

        scalar = ValueFactory.createScalar(value)

        trimRequest = TrimRequest.builder() \
            .setContext(self.cycle) \
            .addScalar(self._amplitude_min, scalar) \
            .setDescription("COI trimming amplitudeMin") \
            .build()

        self.trimService.trimSettings(trimRequest)

    def set_amplitude_max(self, value: float):

        scalar = ValueFactory.createScalar(value)

        trimRequest = TrimRequest.builder() \
            .setContext(self.cycle) \
            .addScalar(self._amplitude_max, scalar) \
            .setDescription("COI trimming amplitudeMax") \
            .build()

        self.trimService.trimSettings(trimRequest)

    # MARGIN LOW
    def set_margin_low(self, value: bool):

        scalar = ValueFactory.createScalar(value)

        trimRequest = TrimRequest.builder() \
            .setContext(self.cycle) \
            .addScalar(self._margin_low_enable, scalar) \
            .setDescription("COI trimming marginLowEnable") \
            .build()

        self.trimService.trimSettings(trimRequest)

    def set_margin_low_min(self, value: float):

        scalar = ValueFactory.createScalar(value)

        trimRequest = TrimRequest.builder() \
            .setContext(self.cycle) \
            .addScalar(self._margin_low_min, scalar) \
            .setDescription("COI trimming marginLowMin") \
            .build()

        self.trimService.trimSettings(trimRequest)

    def set_margin_low_max(self, value: float):

        scalar = ValueFactory.createScalar(value)

        trimRequest = TrimRequest.builder() \
            .setContext(self.cycle) \
            .addScalar(self._margin_low_max, scalar) \
            .setDescription("COI trimming marginLowMax") \
            .build()

        self.trimService.trimSettings(trimRequest)

    # MARGIN HIGH
    def set_margin_high(self, value: bool):
        scalar = ValueFactory.createScalar(value)

        trimRequest = TrimRequest.builder() \
            .setContext(self.cycle) \
            .addScalar(self._margin_high_enable, scalar) \
            .setDescription("COI trimming marginHighEnable") \
            .build()

        self.trimService.trimSettings(trimRequest)

    def set_margin_high_min(self, value: float):
        scalar = ValueFactory.createScalar(value)

        trimRequest = TrimRequest.builder() \
            .setContext(self.cycle) \
            .addScalar(self._margin_high_min, scalar) \
            .setDescription("COI trimming marginHighMin") \
            .build()

        self.trimService.trimSettings(trimRequest)

    def set_margin_high_max(self, value: float):
        scalar = ValueFactory.createScalar(value)

        trimRequest = TrimRequest.builder() \
            .setContext(self.cycle) \
            .addScalar(self._margin_high_max, scalar) \
            .setDescription("COI trimming marginHighMax") \
            .build()

        self.trimService.trimSettings(trimRequest)

    # INTERVAL INFO
    def set_n_subintervals(self, value: int):
        scalar = ValueFactory.createScalar(value)

        trimRequest = TrimRequest.builder() \
            .setContext(self.cycle) \
            .addScalar(self._n_subintevals, scalar) \
            .setDescription("COI trimming nSubIntervals") \
            .build()

        self.trimService.trimSettings(trimRequest)

    def set_start_time(self, value: int):
        scalar = ValueFactory.createScalar(value)

        trimRequest = TrimRequest.builder() \
            .setContext(self.cycle) \
            .addScalar(self._start_time, scalar) \
            .setDescription("COI trimming startTime") \
            .build()

        self.trimService.trimSettings(trimRequest)

    def set_stop_time(self, value: int):
        scalar = ValueFactory.createScalar(value)

        trimRequest = TrimRequest.builder() \
            .setContext(self.cycle) \
            .addScalar(self._stop_time, scalar) \
            .setDescription("COI trimming stopTime") \
            .build()

        self.trimService.trimSettings(trimRequest)

    # NUM. BUNCHES REQUESTED
    def set_n_bunches_requested(self, value: int):
        scalar = ValueFactory.createScalar(value)

        trimRequest = TrimRequest.builder() \
            .setContext(self.cycle) \
            .addScalar(self._n_bunches_requested, scalar) \
            .setDescription("COI trimming nBunchesRequested") \
            .build()

        self.trimService.trimSettings(trimRequest)

    # TARGET BUNCH LENGTH
    def set_target_bunch_length(self, value):
        scalar = ValueFactory.createScalar(value)

        trimRequest = TrimRequest.builder() \
            .setContext(self.cycle) \
            .addScalar(self._target_bunch_length, scalar) \
            .setDescription("COI trimming targetBunchLength") \
            .build()

        self.trimService.trimSettings(trimRequest)

    def set_generator(self, value):

        client = jpype.JPackage("cern.lsa.domain.settings.factory")
        client_value = jpype.JPackage("cern.accsoft.commons.value")

        context = self.context_service.findStandAloneContextByUser(
            self.japc.getSelector()
        )
        if value:
            description = "COI Enabling Generator"
        else:
            description = "COI Disabling Generator"

        trim_builder = (
            client.TrimRequestBuilder().setContext(context).setDescription(description)
        )

        parameter = self._lsa_parameter_service.findParameterByName(
            self.BLOWUP_GENERATOR
        )

        scalar = client_value.ValueFactory.createScalar(jpype.JBoolean(value))
        trim_builder.addScalar(parameter, scalar)

        trim_request = trim_builder.build()
        self._lsa_trim_service.trimSettings(trim_request)
        if value:
            LOG.warning("Generator Enabled")
        else:
            LOG.warning("Generator Disabled")

    def set_params_norm(self, cycle_time: float, norm_values: np.ndarray) -> None:
        """
            Set normalized parameters values.
            Parameters:
                norm_values - array, normalized values to set
                ptype - str, define which params to set, all or to set
        """
        values = self.transform_to_machine_scale(cycle_time, norm_values)
        self.set_params_new(int(cycle_time), values)

    def set_params_new(self, cycle_time: int, values: np.ndarray) -> None:
        """ """
        values = np.round(values, decimals=4)

        # get all current values to calculate delta
        current_values = self.get_params(cycle_time=cycle_time)

        # Define values both for "all" and "to_set" parameters
        future_values = current_values.copy()
        current_values_to_set = np.zeros(np.shape(values))
        future_values_to_set = np.zeros(np.shape(values))

        for param_name in self.params_to_opt_name:
            if param_name in self.params_name:

                idx = self.params_name.index(param_name)
                idx_to_set = self.params_to_opt_name.index(param_name)

                future_values[idx] = values[idx_to_set]
                current_values_to_set[idx_to_set] = current_values[idx]
                future_values_to_set[idx_to_set] = future_values[idx]
            else:
                LOG.warning(f"'{param_name}' is not a parameter to set!")

        # Disable noise generator
        self.set_generator(False)

        skpoint_idx = self.skeleton_points.index(cycle_time)
        initial_values_to_set = self._initial_params_to_set[skpoint_idx]
        delta_values_to_set = np.round(future_values_to_set - initial_values_to_set, decimals=4)
        self.trimmer.set_changes_at(step=skpoint_idx, values=delta_values_to_set)

        # Enable noise generator
        self.set_generator(True)

    def set_param_function(self, param, cycle_time: float, value: float, old_value: float) -> None:
        """
            Set parameters values from LSA function.
            Parameters:
                param - parameter incorporator.
                array - function values to set.
        """
        param.incorporate_and_trim(
            cycle_time=cycle_time,
            value=value,
            relative=False,
            description='Set parameter from COI',
        )

        param.incorporate_and_trim(
            cycle_time=int(self.stop_time) + self.TRIANGLEIR,
            value=old_value,
            relative=False,
            description='Set parameter from COI',
        )

    #############################
    # BUNCH FEATURES EXTRACTION #
    #############################

    def get_measurements(self, index: int, level: float = 0.5) -> dict:
        """ """

        ############################################
        curr_params = self._get_all_params()
        self.meas.set_setting(settings=curr_params)
        ############################################

        is_single_bunch = False

        LOG.info(f"Start getting measurements!")
        beam_info = self.meas.get_measurements(index=index)
        # acq_time, n_bunches, waveform, bunch_position, injection_info
        LOG.info(f"End getting measurements!")

        measurements = {}
        for n in list(beam_info.keys()):
            curr_beam_info = beam_info[n]
            n_bunches = curr_beam_info["n_bunches"]
            waveform = curr_beam_info["waveform"]
            bunch_position = curr_beam_info["bunch_position"]

            if n_bunches == 0:
                LOG.warning(f"No beam at time: {curr_beam_info['acquisition_time']}")
                timescale = np.nan
                bunches = np.nan
                features_all = np.array([[np.nan, np.nan, np.nan, np.nan]])

            else:

                if n_bunches != self.n_bunches_requested:
                    LOG.warning(f"Incomplete beam at time: {curr_beam_info['acquisition_time']}")

                bunches = self.an.get_bunches(n_bunches, waveform, bunch_position)

                if n_bunches == 1:
                    is_single_bunch = True

                bunch_samples = np.shape(bunches)[1]
                timescale = np.arange(bunch_samples) * self.dtime

                features_all = []
                for n_bunch in range(n_bunches):
                    n_features = self.an.get_width(timescale, bunches[n_bunch], level)
                    features_all.append(n_features)

                if n_bunches == 0:
                    n_features = list(np.zeros(self.n_bunches_requested) * np.nan)
                    features_all.append(n_features)

                features_all = np.array(features_all)

            measurements[n] = {
                **beam_info[n],
                "is_single_bunch": is_single_bunch,
                "timescale": timescale,
                "bunches": bunches,
                "width": features_all[:, 0],
                "center": features_all[:, 1],
                "sigma": features_all[:, 2],
                "bunch_length": features_all[:, 3],
            }

        return measurements

    #####################
    # CONSTRAIN METHODS #
    #####################

    @staticmethod
    def clip_to_space(array: np.ndarray, space: Box) -> np.ndarray:
        """
            Clip an array to the given space.
        """
        return np.clip(array, space.low, space.high)

    def transform_to_machine_scale(
            self,
            cycle_time: float,
            norm_values: np.ndarray,
    ) -> np.ndarray:
        """
            Transform normalized values to machine scale.
            Parameters:
                norm_values - array, parameters values normalized
                ptype - str, define which params to get, all or to set
        """
        box = self.params_to_opt_box
        param_names = self.params_to_opt_name
        scaler = self.params_to_opt_scaler

        values = scaler.unscale(norm_values)

        # check margin low minor than margin high
        values = self.margin_constrain(values, param_names, cycle_time)

        values = self.clip_to_space(values, box)
        return values

    def margin_constrain(self, values: np.ndarray, param_names, cycle_time) -> np.ndarray:
        """
            Check that margin low minor than margin high always.
            Parameters:
                values - array, values to check.
                param_names - list, parameters names.
                cycle_time - float, current skeleton point.
            Returns:
                values - array, values to be set.
        """
        values = np.reshape(values, (len(param_names), 1))
        init_values = self.initial_params.copy()

        index = self.skeleton_points.index(cycle_time)

        margin_names = ['margin_low', 'margin_high']
        margins_set = set(margin_names)
        params_set = set(param_names)
        common_names = margins_set.intersection(params_set)

        if margins_set.issubset(common_names):
            margin_idxs = [
                param_names.index(margin_name) for margin_name in margin_names
            ]
            margin_values = [
                values[idx] for idx in margin_idxs
            ]

            margin_gap = margin_values[1][0] - self.min_margins_distance
            if margin_values[0][0] > margin_gap:
                if margin_gap <= self.margins_box.low[0][0]:
                    margin_values[1][0] = margin_values[0][0] + self.min_margins_distance
                else:
                    margin_values[0][0] = margin_gap

            for margin_idx, margin_value in zip(margin_idxs, margin_values):
                values[margin_idx] = margin_value

        elif 'margin_low' in common_names:
            margin_idx = [param_names.index('margin_low')]
            margin_values = [values[margin_idx]]

            values[margin_idx] = np.clip(
                margin_values,
                None,
                init_values[index][-1] - self.min_margins_distance
            )

        elif 'margin_high' in common_names:
            margin_idx = [param_names.index('margin_high')]
            margin_values = [values[margin_idx]]

            values[margin_idx] = np.clip(
                margin_values,
                init_values[index][-2] + self.min_margins_distance,
                None
            )

        values = np.reshape(values, (len(param_names)))
        values = np.round(values, decimals=4)
        return values
