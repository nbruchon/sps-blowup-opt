# Automated Optimization of Controlled Longitudinal Emittance Blow-Up
# Developed by N. Bruchon in 2022

# IMPORTING #
###################################################################################
import logging
import typing as t
# import time
from datetime import datetime as dt
import numpy as np
from types import SimpleNamespace

from cernml import coi
from cernml.mpl_utils import FigureRenderer, make_renderer

# from gym import spaces
from gym.spaces import Box

from ._machine import Communicator

from ._tmp_save import Save

from pyjapc import PyJapc
from cernml.coi.cancellation import Token
# from cernml.lsa_utils._incorporator import Incorporator

import gym

if t.TYPE_CHECKING:
    from matplotlib.axes import Axes
    from matplotlib.figure import Figure
    from matplotlib.lines import Line2D

InfoDict = t.Dict[str, t.Any]

LOG = logging.getLogger(__name__)


# ADDITIONAL METHODS
###################################################################################
def clip_to_space(array: np.ndarray, space: Box) -> np.ndarray:
    """ Clip an array so that the result lies within the given space. """
    return np.clip(array, space.low, space.high)


# TODO: probably useless: think to remove it...
def does_relim_increase_y_size(axes: "Axes") -> bool:
    """ Relim an axes and check if the Y axis has grown. """
    old_ymin = axes.dataLim.ymin
    old_ymax = axes.dataLim.ymax
    axes.relim()
    axes.dataLim.update_from_data_xy([(1.0, 0.0)], ignore=False)
    new_ymin = axes.dataLim.ymin
    new_ymax = axes.dataLim.ymax
    # Also check if the old bounds were infinite. This happens when the
    # plot is initially empty.
    needs_update = (
        (new_ymin < old_ymin)
        or (new_ymax > old_ymax)
        or not np.isfinite(old_ymin)
        or not np.isfinite(old_ymax)
    )
    return needs_update


# CLASS
###################################################################################
class BaseEnv(coi.FunctionOptimizable, gym.Env, coi.Configurable):
    """
        Blow-Up settings for SPS.
    """

    metadata = {
        "render.modes": ["ansi", "human", "matplotlib_figures"],
        "cern.machine": coi.Machine.SPS,
    }

    render: FigureRenderer
    """ Renderer used for plotting """

    communicator: Communicator
    """ Communicator used for talking to SPS """

    optimization_space: Box

    # OBJECTIVE FUNCTIONS
    ################################################################################
    objective_functions: t.Optional[t.Tuple[str, ...]] = (
        "RMSE",
    )

    _values: SimpleNamespace

    # OBS #

    features: dict = dict()
    features_min: dict = dict()
    features_max: dict = dict()
    is_single_bunch: bool

    # COST #
    beam_losses: float = 0.0
    costs: float = 0.0

    # MISC #
    init_check: bool = True

    cnt: int = 0
    cost_values: list = list()
    cost_values_initial: list

    sav: Save

    amplitude: np.ndarray
    margin_high: np.ndarray
    margin_low: np.ndarray

    _tmp_settings: list

    measurements: dict

    min_bunch_length: np.ndarray
    mean_bunch_length: np.ndarray
    max_bunch_length: np.ndarray

    min_bunch_length_initial: np.ndarray
    mean_bunch_length_initial: np.ndarray
    max_bunch_length_initial: np.ndarray

    min_bunch_lengths: np.ndarray
    mean_bunch_lengths: np.ndarray
    max_bunch_lengths: np.ndarray

    index: int

    time_start: int
    time_stop: int
    num_steps: int
    target_sigma: float
    target_tolerance: float
    bunch_tolerance: int
    objective_function: str

    ################################################################################

    # CONSTRUCTOR
    ################################################################################
    def __init__(self, communicator: Communicator):
        """
            BaseEnv initialization
            Communicator class required
        """
        self._communicator: Communicator = communicator

        self.renderer = make_renderer(self.render_settings)

        # Method to re-initialize class parameters
        self.cnt = 0
        self.measurements = dict()
        self.beam_losses = 0
        self.costs = 0
        self._tmp_settings = []

        # Render re-initialization
        self.renderer = make_renderer(self.render_settings)

        # update settings
        # Blow-up interval
        self.time_start = self.communicator.start_time
        self.time_stop = self.communicator.stop_time
        self.num_steps = self.communicator.n_subintervals

        # Target
        self.target_sigma = self.communicator.target_bunch_length
        self.target_tolerance = self.communicator.target_tolerance
        self.bunch_tolerance = self.communicator.n_bunches_discard
        self.objective_function = self.communicator.cost_function

        # TODO: put it to 0, in the case initially the beam does not reach extraction
        # Optimization
        self.index = self.num_steps

        # optimization space re-definition
        self.optimization_space = Box(
            low=-1.0,
            high=1.0,
            shape=(self.num_params_to_opt,),
        )

    ################################################################################

    # PROPERTIES
    ################################################################################
    @property
    def communicator(self) -> Communicator:
        """ """
        return self._communicator

    @property
    def simulation(self) -> bool:
        """ """
        return self.communicator.simulation

    @property
    def japc(self) -> PyJapc:
        """ """
        return self.communicator.japc

    @property
    def token(self) -> Token:
        """ """
        return self.communicator.token

    @property
    def machine(self) -> str:
        """ """
        return self.communicator.user.split('.')[0]

    @property
    def cycle(self) -> str:
        """ """
        return self.communicator.user.split('.')[-1]

    @property
    def objective_function_available_names(self) -> list:
        """ """
        return self.communicator.cost_function_items

    @property
    def first_injection(self) -> int:
        """ """
        return int(self.communicator.first_injection_time)

    @property
    def skeleton_points(self) -> t.Tuple[int, ...]:
        """ """
        return self.communicator.skeleton_points

    @property
    def skeleton_points_inj(self) -> np.ndarray:
        """ """
        skeleton_points_inj = [
            self.skeleton_points[idx] - self.first_injection for idx in range(self.num_steps)
        ]
        return np.array(skeleton_points_inj)

    @property
    def observation_points(self) -> t.Tuple[int, ...]:
        """ """
        return self.communicator.observation_points

    @property
    def parameter_names(self) -> t.ClassVar[t.Tuple[str, ...]]:
        """ """
        return self.communicator.parameter_names

    @property
    def num_params_to_opt(self) -> int:
        """ """
        return self.communicator.num_parameters_to_opt

    @property
    def savedata(self) -> float:
        """ """
        try:
            save_data = self._values.savedata
        except AttributeError:
            save_data = self.communicator.savedata
        return save_data

    @property
    def render_settings(self) -> dict:
        render_settings = {
            "Setting up blow-up noise...": self.plotting,
        }
        return render_settings

    # Method in FunctionOptimizable
    def get_optimization_space(self, cycle_time: float) -> gym.Space:
        return self.optimization_space

    ################################################################################

    # CONFIGURATION
    ################################################################################
    def get_config(self) -> coi.Config:
        """
            Enable configuration of Communicator settings on the GUI

            config = coi.Config()
            config.add(
                dest='',
                value=,
                label='',
                help=None,
                type=,
                range=(,),
                choices=None,
                default=
            )
        """
        config = coi.Config()

        # Target bunch length
        config.add(
            dest='target_bunch_length',
            value=self.communicator.target_bunch_length,
            default=2.0,
            label='Target bunch length [ns]'
        )

        # Start time
        config.add(
            dest='start_time',
            value=self.communicator.start_time_injection,
            default=15000,
            label='Start time [ms]'
        )

        # Stop time
        config.add(
            dest='stop_time',
            value=self.communicator.stop_time_injection,
            default=19000,
            label='Stop time [ms]'
        )

        # Num. sub-intervals
        config.add(
            dest='n_subintervals',
            value=self.communicator.n_subintervals,
            default=1,
            label='Num. sub-intervals'
        )

        # Num. bunches requested
        config.add(
            dest='n_bunches_requested',
            value=self.communicator.n_bunches_requested,
            default=3,
            label='Num. bunches requested'
        )

        # Amplitude checkbox
        config.add(
            dest='opt_amplitude',
            value=self.communicator.amplitude_enable,
            default=False,
            label='Amplitude'
        )

        # Amplitude range min.
        config.add(
            dest='range_min_amplitude',
            value=self.communicator.amplitude_min,
            default=0.0,
            label='Amplitude range min.'
        )

        # Amplitude range max.
        config.add(
            dest='range_max_amplitude',
            value=self.communicator.amplitude_max,
            default=1.0,
            label='Amplitude range max.'
        )

        # Margin low checkbox
        config.add(
            dest='opt_margin_low',
            value=self.communicator.margin_low_enable,
            default=False,
            label='Margin Low'
        )

        # margin_low range min.
        config.add(
            dest='range_min_margin_low',
            value=self.communicator.margin_low_min,
            default=0.0,
            label='Margin low range min.'
        )

        # margin_low range max.
        config.add(
            dest='range_max_margin_low',
            value=self.communicator.margin_low_max,
            default=1.0,
            label='Margin low range max.'
        )

        # Margin high checkbox
        config.add(
            dest='opt_margin_high',
            value=self.communicator.margin_high_enable,
            default=False,
            label='Margin high'
        )

        # margin_high range min.
        config.add(
            dest='range_min_margin_high',
            value=self.communicator.margin_high_min,
            default=0.0,
            label='Margin high range min.'
        )

        # margin_high range max.
        config.add(
            dest='range_max_margin_high',
            value=self.communicator.margin_high_max,
            default=1.0,
            label='Margin high range max.'
        )

        # Saving checkbox
        config.add(
            dest='savedata',
            value=self.savedata,
            default=False,
            label='Save data',
        )

        # # Get skeleton points in config tab
        # # solved issue in the GeOFF repository
        # # https://gitlab.cern.ch/geoff/geoff-app/-/issues/30
        # config.add(
        #     dest='skpoint_list',
        #     value=' '.join([str(e) for e in self.skeleton_points]),
        #     label='Skeleton points',
        #     type=str,
        # )
        # config.add(
        #     'skpoint_list',
        #     self.skeleton_points,
        #     range(1, 10)
        # )

        return config

    def apply_config(self, values: SimpleNamespace) -> None:
        """
            Apply configurations set in the GUI
        """

        _previous_skeleton_points = np.array(self.skeleton_points).copy()

        self._values = values

        # AMPLITUDE
        if not values.opt_amplitude == self.communicator.amplitude_enable:
            self.communicator.set_amplitude(values.opt_amplitude)

        if not values.range_min_amplitude == self.communicator.amplitude_min:
            self.communicator.set_amplitude_min(values.range_min_amplitude)

        if not values.range_max_amplitude == self.communicator.amplitude_max:
            self.communicator.set_amplitude_max(values.range_max_amplitude)

        # MARGIN LOW
        if not values.opt_margin_low == self.communicator.margin_low_enable:
            self.communicator.set_margin_low(values.opt_margin_low)

        if not values.range_min_margin_low == self.communicator.margin_low_min:
            self.communicator.set_margin_low_min(values.range_min_margin_low)

        if not values.range_max_margin_low == self.communicator.margin_low_max:
            self.communicator.set_margin_low_max(values.range_max_margin_low)

        # MARGIN HIGH
        if not values.opt_margin_high == self.communicator.margin_high_enable:
            self.communicator.set_margin_high(values.opt_margin_high)

        if not values.range_min_margin_high == self.communicator.margin_high_min:
            self.communicator.set_margin_high_min(values.range_min_margin_high)

        if not values.range_max_margin_high == self.communicator.margin_high_max:
            self.communicator.set_margin_high_max(values.range_max_margin_high)

        # INTERVAL DURATION
        if not values.n_subintervals == self.communicator.n_subintervals:
            self.communicator.set_n_subintervals(values.n_subintervals)

        if not values.start_time == self.communicator.start_time_injection:
            self.communicator.set_start_time(values.start_time)

        if not values.stop_time == self.communicator.stop_time_injection:
            self.communicator.set_stop_time(values.stop_time)

        # NUM. BUNCHES REQUESTED
        if not values.n_bunches_requested == self.communicator.n_bunches_requested:
            self.communicator.set_n_bunches_requested(values.n_bunches_requested)

        # TARGET BUNCH LENGTH
        if not values.target_bunch_length == self.communicator.target_bunch_length:
            LOG.warning(f"target bunch length: {values.target_bunch_length}")
            LOG.warning(f"type: {type(values.target_bunch_length)}")
            LOG.warning(f"target bunch length: {self.communicator.target_bunch_length}")
            LOG.warning(f"type: {type(self.communicator.target_bunch_length)}")
            self.communicator.set_target_bunch_length(values.target_bunch_length)

        # Save data
        if values.savedata:

            self.sav = Save(
                machine=self.machine,
                operation="BLOW_UP",
                cycle=self.cycle
            )

        # Close communicator before reopening it
        self.communicator.close()

        # Re-open communicator
        self._communicator = Communicator(
            japc=self.japc,
            token=self.token,
            simulation=self.simulation,
        )

        # Method to re-initialize class parameters
        self.cnt = 0
        self.measurements = dict()
        self.beam_losses = 0
        self.costs = 0
        self._tmp_settings = []

        # Check that skeleton points have been manually updated
        if not np.array_equiv(_previous_skeleton_points, np.array(self.skeleton_points)):
            LOG.error(f"Update config with new skeleton points")

        # Render re-initialization
        self.renderer = make_renderer(self.render_settings)

        # update settings
        # Blow-up interval
        self.time_start = self.communicator.start_time
        self.time_stop = self.communicator.stop_time
        self.num_steps = self.communicator.n_subintervals

        # Target
        self.target_sigma = self.communicator.target_bunch_length
        self.target_tolerance = self.communicator.target_tolerance
        self.bunch_tolerance = self.communicator.n_bunches_discard
        self.objective_function = self.communicator.cost_function

        # Optimization
        self.index = self.num_steps

        # optimization space re-definition
        self.optimization_space = Box(
            low=-1.0,
            high=1.0,
            shape=(self.num_params_to_opt,),
        )

    def override_skeleton_points(self) -> t.List[int]:
        """ """
        return list(self.skeleton_points)

    ################################################################################

    # CUSTOM METHODS
    ################################################################################
    def _update_settings(self) -> None:
        """ """

        # get blow-up settings from LSA
        values = self._get_settings()
        self.amplitude, self.margin_low, self.margin_high = values.transpose()

        self._tmp_settings.append(values[self.index].tolist())

        # get SPS measurements from ABWLM and BQM
        _measurements = self._get_measurements()

        _min_bunch_length, _mean_bunch_length, _max_bunch_length = [], [], []
        _cost = []
        for n in range(self.num_steps):
            _bunch_lengths = _measurements[str(n)]['bunch_length'] * 1e9
            _min_bunch_length.append(np.min(_bunch_lengths))
            _mean_bunch_length.append(np.mean(_bunch_lengths))
            _max_bunch_length.append(np.max(_bunch_lengths))

            # Add settings to _curr_measurements
            _measurements[str(n)]['amplitude'] = self.amplitude[n]
            _measurements[str(n)]['margin_low'] = self.margin_low[n]
            _measurements[str(n)]['margin_high'] = self.margin_high[n]

            if self.objective_function == "RMSE":
                _measurements[str(n)]['cost'] = self._get_rmse_new(_bunch_lengths)
                _cost.append(self._get_rmse_new(_bunch_lengths))

        self.min_bunch_length = np.array(_min_bunch_length)
        self.mean_bunch_length = np.array(_mean_bunch_length)
        self.max_bunch_length = np.array(_max_bunch_length)

        _measurements['current_time'] = dt.now().strftime("%D_%T")

        _measurements['min_bunch_length'] = self.min_bunch_length
        _measurements['mean_bunch_length'] = self.mean_bunch_length
        _measurements['max_bunch_length'] = self.max_bunch_length
        _measurements['cost'] = _cost

        if not self.simulation:
            all_bunch_lengths = self.communicator.all_bunchlengths
            self.all_bunch_lengths = all_bunch_lengths
            self.min_bunch_lengths = np.array(all_bunch_lengths[1]) * 1e9
            self.mean_bunch_lengths = np.array(all_bunch_lengths[2]) * 1e9
            self.max_bunch_lengths = np.array(all_bunch_lengths[3]) * 1e9

        self.measurements = _measurements.copy()

        self.cost_values = _cost

        if self.savedata:

            if self.cnt < 10:
                n_iter = '0' + str(self.cnt)
            else:
                n_iter = str(self.cnt)
            self.sav.save_dict_to_group(groupname='iter_' + n_iter, dictionary=_measurements)

        # Update iter counter
        self.cnt += 1

    def _get_settings(self) -> np.ndarray:
        """ """
        return self.communicator._get_all_params()

    def _get_measurements(self) -> dict:
        """ """
        return self.communicator.get_measurements(self.index)

    def _get_rmse_new(self, bunch_length: np.ndarray) -> float:
        """ """
        if all(np.isnan(bunch_length)):
            cost = np.nan
        else:
            if len(bunch_length) > 1 and self.bunch_tolerance > 0.0:
                ext_indexes = np.argsort(
                    np.abs(bunch_length - self.target_sigma)
                )
                bunch_length = np.delete(bunch_length, ext_indexes[-self.bunch_tolerance])

            out_indexes = np.where(
                np.logical_or(
                    bunch_length < self.target_sigma * (1 - self.target_tolerance),
                    bunch_length > self.target_sigma * (1 + self.target_tolerance)
                )
            )[0]

            filtered_bunch_length = bunch_length[out_indexes]
            err = filtered_bunch_length / self.target_sigma - 1

            cost = np.sqrt(np.sum(err ** 2) / len(bunch_length))
        return cost

    ################################################################################

    # SINGLE OPTIMIZATION METHOD
    ################################################################################
    # TODO: check the following methods
    def get_initial_params(self, cycle_time: float) -> np.ndarray:
        """ """
        idx = self.skeleton_points.index(cycle_time)
        initial_params = self.communicator.initial_params_to_set[idx]
        LOG.info(f"Initial params: {initial_params}")
        return initial_params

    # TODO: clean the following method
    def compute_function_objective(self, cycle_time: float, params: np.ndarray) -> float:
        """
            Perform an optimization step.
        """
        #
        if self.cnt == 0:
            # TODO: read here the parameters from LSA to keep them constant during the optimization
            #  even in the case they are manually changed during the optimization
            self.measurements = dict()
            self.beam_losses = 0
            self.costs = 0
            self._tmp_settings = []
        try:
            # Find which skeleton point is being optimized
            index = self.communicator.skeleton_points.index(int(cycle_time))
            self.index = index
            # Get the corresponding observation point
            obs_time = self.observation_points[index]

            LOG.info(f"Iter. num. {self.cnt}, "
                     f"Cycle Time: {cycle_time}, "
                     f"Observation Time: {obs_time}, "
                     f"Index: {index}")

            # Initial status of the machine
            if self.cnt == 0:
                LOG.warning(f"Initial acquisitions")
                self._update_settings()
                self.min_bunch_length_initial = self.min_bunch_length.copy()
                self.mean_bunch_length_initial = self.mean_bunch_length.copy()
                self.max_bunch_length_initial = self.max_bunch_length.copy()
                self.cost_values_initial = self.cost_values.copy()

            # Writing new settings to LSA
            params = np.round(params, decimals=3)
            settings_to_write = clip_to_space(params, self.optimization_space)
            self.communicator.set_params_norm(
                cycle_time=cycle_time,
                norm_values=settings_to_write
            )

            # Reading new settings from LSA
            current_settings_norm = np.round(
                self.communicator.get_params_norm(cycle_time=cycle_time),
                decimals=3
            )
            if not np.array_equiv(settings_to_write, current_settings_norm[self.communicator.to_optimize]):
                LOG.error(f"Mismatch between written {settings_to_write} "
                          f"(dtype: {settings_to_write.dtype}) "
                          f"and read settings {current_settings_norm} "
                          f"(dtype: {current_settings_norm.dtype})!")

            self._update_settings()
            LOG.info(
                f"BlowUp Settings:\n"
                f"    . {self.parameter_names[0]}: {self.amplitude}\n"
                f"    . {self.parameter_names[1]}: {self.margin_low}\n"
                f"    . {self.parameter_names[2]}: {self.margin_high}"
            )
            self.beam_losses = self.cost_values[index]

        except coi.cancellation.CancelledError:
            LOG.error(f"Called the token complete cancellation!")
            self.token.complete_cancellation()
            raise

        # TODO: collect here all the information to be plotted later
        #       the same values should be saved is savedata == True
        # TODO: think how to differ saving between single optimization and RL
        #
        # Data to be saved:
        #   1. Blow up settings:
        #       - amplitude
        #       - margin low
        #       - margin high
        #   2. Cost function:
        #       - bunch-length factor
        #       - bunch-shape factor
        #   3. Features:
        #       - profiles
        #       - timescales
        #       - widths
        #       - time labels
        #       - sigma
        #       - bunch-length
        #       - center
        #       - e chi più ne ha più ne metta...

        return self.beam_losses

    ################################################################################

    # REINFORCEMENT LEARNING METHODS
    ################################################################################
    def reset(self) -> np.array:
        LOG.info("Resetting environment:")
        reset_action = self.action_space.sample()
        self._take_action(reset_action)
        obs = self._get_state()
        return obs

    def step(self, action: np.ndarray) -> tuple:
        info = {}
        obs = self.compute_observation(action, info)
        rew = self.compute_reward(obs, None, info)
        done = self.compute_done(obs, rew, info)
        return obs, rew, done, info

    def compute_observation(self, action: np.ndarray, info: InfoDict) -> np.array:

        return np.random.rand(1)

    def compute_reward(self, obs: np.ndarray, goal: None, info: InfoDict) -> float:

        return np.random.rand(1)[0]

    def compute_done(self, obs: np.ndarray, reward: float, info: InfoDict) -> bool:
        done = True
        return done

    def _take_action(self, kicks: np.ndarray) -> None:

        return None

    # Get State
    def _get_state(self) -> np.ndarray:

        return np.random.rand(1)

    # Seed
    def seed(self, seed: t.Optional[int] = None) -> None:
        return None

    ################################################################################

    # COMMUNICATOR CLOSE
    ################################################################################
    def close(self) -> None:
        """
            perform any necessary cleanup.
        """
        self.communicator.close()
        LOG.error("Just to see when it is called")
        return None

    ################################################################################

    # VISUALIZATION
    ################################################################################
    def render(self, mode: str = "human") -> t.Any:
        """
            Render the environment.
            Parameters:
                mode - the mode to render with.
        """
        if mode in ("human", "matplotlib_figures"):
            return self.renderer.update(mode)
        if mode == "ansi":
            return repr(self)
        return super().render(mode)

    def plotting(self, figure: "Figure") -> None:

        # Bunch length plot
        ############################################################################
        ax_1: "Axes" = figure.add_subplot(2, 1, 1)
        ax_1.set(
            xlim=[
                self.communicator.ramp_start_injection,
                self.communicator.flat_top_start_injection
            ],
            ylim=[1.5, 3.0],
            ylabel="Bunch length [ns]"
        )
        ax_1.xaxis.set_ticks(
            np.hstack([
                self.communicator.ramp_start_injection,
                self.observation_points,
                self.communicator.flat_top_start_injection
            ])
        )
        labels = [str(item.get_text()) for item in ax_1.get_xticklabels()]
        labels[0] = "Ramp Start"
        labels[1:-1] = [str(item) for item in self.observation_points]
        labels[-1] = "Ramp End"
        ax_1.set_xticklabels(labels)
        faces = [ax_1.axvspan(i-100, i+100, facecolor='w', alpha=0.2) for i in self.observation_points]
        ax_1.tick_params('x', labelbottom=False)

        # Plot observation points
        [ax_1.axvline(x=obs, ls=':', c='k') for obs in self.observation_points]

        # Plot target bunch length
        ln_h = ax_1.axhline(
            y=self.target_sigma, linestyle='--', color='k', label="Target bunch length"
        )
        ln_hs = ax_1.axhspan(
            (1 - self.target_tolerance) * self.target_sigma,
            (1 + self.target_tolerance) * self.target_sigma,
            facecolor='b', alpha=0.20, label="Tolerance"
        )

        # Plot bunch lengths
        ln_min: "Line2D"
        ln_min, = ax_1.plot(
            [], [],
            ls='', marker='^', c='k', label="Min. bunch length"
        )
        ln_mean: "Line2D"
        ln_mean, = ax_1.plot(
            [], [],
            ls='', marker='d', c='k', label="Mean bunch length"
        )
        ln_max: "Line2D"
        ln_max, = ax_1.plot(
            [], [],
            ls='', marker='v', c='k', label="Max. bunch length"
        )

        # Plot initial bunch lengths
        ln_min_initial: "Line2D"
        ln_min_initial, = ax_1.plot(
            [], [],
            ls='', marker='^', c='k', alpha=0.25, label="Initial min. bunch length"
        )
        ln_mean_initial: "Line2D"
        ln_mean_initial, = ax_1.plot(
            [], [],
            ls='', marker='d', c='k', alpha=0.25, label="Initial mean bunch length"
        )
        ln_max_initial: "Line2D"
        ln_max_initial, = ax_1.plot(
            [], [],
            ls='', marker='v', c='k', alpha=0.25, label="Initial max. bunch length"
        )

        # Plot all bunch lengths
        ln_min_all: "Line2D"
        ln_min_all, = ax_1.plot(
            [], [],
            ls=':', marker='', c='k', alpha=0.75, label="Min. bunch length"
        )
        ln_mean_all: "Line2D"
        ln_mean_all, = ax_1.plot(
            [], [],
            ls=':', marker='', c='k', alpha=0.75, label="Mean bunch length"
        )
        ln_max_all: "Line2D"
        ln_max_all, = ax_1.plot(
            [], [],
            ls=':', marker='', c='k', alpha=0.75, label="Max. bunch length"
        )

        # Cost function plot
        ############################################################################
        ax_2: "Axes" = figure.add_subplot(2, 1, 2, sharex=ax_1)
        ax_2.set(
            ylim=[-0.20, 1.25],
            xlabel="Injection time [ms]",
            ylabel="Cost function [au]"
        )

        # Plot observation points
        [ax_2.axvline(x=obs, ls=':', c='k') for obs in self.observation_points]

        # Plot reference
        ax_2.axhline(y=0.0, linestyle='--', color='k', label="")

        # Plot cost function
        ln_cost: "Line2D"
        ln_cost, = ax_2.plot(
            [], [],
            ls='', marker='o', c='r', label="Cost function"
        )

        # Plot initial cost function
        ln_cost_initial: "Line2D"
        ln_cost_initial, = ax_2.plot(
            [], [],
            ls='', marker='s', color='r', alpha=0.2, label="Initial cost function"
        )

        ln_cost_all: "Line2D"
        ln_cost_all, = ax_2.plot(
            [], [],
            ls=':', marker='', color='r', alpha=0.75, label="Cost function"
        )

        # Plots update
        ############################################################################
        while True:
            yield

            if self.cnt == 2:

                ln_h.set_ydata(
                    self.target_sigma
                )
                xy = ln_hs.get_xy()
                xy[:, 1] = [
                    (1 - self.target_tolerance) * self.target_sigma,
                    (1 + self.target_tolerance) * self.target_sigma,
                    (1 + self.target_tolerance) * self.target_sigma,
                    (1 - self.target_tolerance) * self.target_sigma,
                    (1 - self.target_tolerance) * self.target_sigma,
                ]
                ln_hs.set_xy(xy=xy)

                ln_min_initial.set_data(
                    self.observation_points, self.min_bunch_length_initial
                )
                ln_mean_initial.set_data(
                    self.observation_points, self.mean_bunch_length_initial
                )
                ln_max_initial.set_data(
                    self.observation_points, self.max_bunch_length_initial
                )

                ln_cost_initial.set_data(
                    self.observation_points, self.cost_values_initial
                )

                # Update visualization for the first observation point
                for idx in range(len(faces)):
                    faces[idx].set_facecolor('w')

                faces[self.index].set_facecolor('r')
                ax_2.get_xticklabels()[1:-1][self.index].set_fontsize(14)

            if self.cnt >= 2:
                # Update visualization of observation points
                if self._tmp_settings[-1] in self._tmp_settings[1:-1]:
                    ax_2.get_xticklabels()[1:-1][self.index].set_fontsize(12)
                    faces[self.index].set_facecolor('g')

                    if self.index < self.num_steps - 1:
                        faces[self.index + 1].set_facecolor('r')
                        ax_2.get_xticklabels()[1:-1][self.index + 1].set_fontsize(14)
                    else:
                        self.cnt = 0

                    self._tmp_settings = []

                if not self.simulation:
                    acq_times = np.array(self.all_bunch_lengths[0])

                    t0 = self.communicator.ramp_start_injection
                    t1 = self.communicator.flat_top_start_injection
                    mask = np.where((acq_times >= t0) & (acq_times <= t1))[0]

                    ln_min_all.set_data(acq_times[mask], self.min_bunch_lengths[mask])
                    ln_mean_all.set_data(acq_times[mask], self.mean_bunch_lengths[mask])
                    ln_max_all.set_data(acq_times[mask], self.max_bunch_lengths[mask])

                    _all_bunch_lengths = np.array(self.all_bunch_lengths[1:])*1e9
                    all_costs = []
                    for i in range(len(acq_times)):
                        bl = _all_bunch_lengths[:, i]
                        idx = np.where(
                            np.logical_or(
                                bl < self.target_sigma * (1 - self.target_tolerance),
                                bl > self.target_sigma * (1 + self.target_tolerance)
                            )
                        )[0]
                        err = bl[idx] / self.target_sigma - 1
                        all_costs.append(np.sqrt(np.sum(err**2) / len(bl)))

                    LOG.info(f"len(acq_times): {len(acq_times)}")
                    LOG.info(f"len(all_costs): {len(all_costs)}")
                    LOG.info(f"mask: {mask}")

                    ln_cost_all.set_data(acq_times[mask], np.array(all_costs)[mask])

                # Update bunch length plot
                ln_min.set_data(self.observation_points, self.min_bunch_length)
                ln_mean.set_data(self.observation_points, self.mean_bunch_length)
                ln_max.set_data(self.observation_points, self.max_bunch_length)

                # Update cost function plot
                ln_cost.set_data(self.observation_points, self.cost_values)

    ################################################################################
