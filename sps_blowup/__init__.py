# Automated Optimization of Controlled Longitudinal Emittance Blow-Up
# Developed by N. Bruchon in 2022

#############
# IMPORTING #
#############
import typing as _t

from cernml.coi import register as _register
from cernml.coi.cancellation import Token as _CancellationToken

from ._baseenv import BaseEnv
from ._machine import Communicator

if _t.TYPE_CHECKING:
    from pyjapc import PyJapc


#########
# CLASS #
#########


class GeneralEnv(BaseEnv):
    """
        General environment for SPS-blowup optimization
    """

    metadata = dict(
        BaseEnv.metadata, **{"cern.japc": True, "cern.cancellable": True}
    )

    def __init__(
            self,
            japc: "PyJapc",
            cancellation_token: _t.Optional[_CancellationToken] = None,
            *,
            simulation: bool = True,
    ) -> None:
        if cancellation_token is None:
            cancellation_token = _CancellationToken()

        comm = Communicator(
            japc,
            cancellation_token,
            simulation=simulation,
        )

        super().__init__(comm)


################
# REGISTRATION #
################


# simulation
_register(
    "SPS-BlowUp-Simulated-v0",
    entry_point=GeneralEnv,
    kwargs={
        'simulation': True,
    }
)

# SPS
_register(
    "SPS-BlowUp-Real-v0",
    entry_point=GeneralEnv,
    kwargs={
        'simulation': False,
    }
)
