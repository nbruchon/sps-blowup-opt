# Operational Tool for Automatic Setup of Controlled Longitudinal Emittance Blow-Up in the CERN SPS

### Overview

The controlled longitudinal emittance blow-up is a critical
step in ensuring the stability of high-intensity LHC-type beams
in the CERN SPS. This repository contains an implementation of
the [cernml-coi](https://gitlab.cern.ch/geoff/cernml-coi)
interface to determine optimal settings quickly and efficiently.

The proposed approach involves splitting the blow-up process
into multiple sub-intervals and optimizing the noise parameters
by observing the longitudinal profiles at the end of each
sub-interval. The derived bunch lengths are then used to determine
the objective function, which measures the error with respect to
the requirements.

### Getting Started

- Set the parameters in LSA
- Run GeOFF


### Usage
To run this environment on your VPC, follow the steps below:

1. Clone the repository.
2. Set the settings for the optimization process in the LSA file.
3. Run: ```acc-py app run acc-app-optimisation path/to/the/package```.

For help: ```acc-py app run acc-app-optimisation --help```.

Options:
- ```--lsa-server=next```: to run on NEXT database.
- ```--lsa-server=sps```: to run on PRO database.
- ```--machine=sps```: to set machine.
- ```--japc-no-set```:
- ```--no-capture-stout```:

The approach will optimize the noise parameters based on the settings
stored in LSA. The approach will try to keep the bunch length constant
in each interval, which contributes to the stability of high-intensity
beams in the CERN SPS.

Commands:

Without interacting with SPS
```
acc-py app run acc-app-optimisation --lsa-server=next --no-capture-stdout --japc-no-set ~/Projects/sps-blowup/sps_blowup
```

Interacting with SPS
```
acc-py app run acc-app-optimisation --lsa-server=sps --no-capture-stdout --japc-no-set ~/Projects/sps-blowup/sps_blowup
```

Without interacting with SPS
```
python -m acc_app_optimisation --lsa-server=next --no-capture-stdout --japc-no-set ~/Projects/sps-blowup/sps_blowup
```
Since h5py is not available in COI. (now fixed)


### Notes

You can use [Github-flavored Markdown](https://guides.github.com/features/mastering-markdown/)
to write your content.
